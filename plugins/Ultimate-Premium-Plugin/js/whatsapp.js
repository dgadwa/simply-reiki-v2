jQuery('.clWhatsapp').each(function() {
	
	// Get title to be shared
	var title = encodeURIComponent(jQuery(this).attr('data-text'));
	
	// Get link to be shared	
	var link  = encodeURIComponent(jQuery(this).attr('data-url'));
	
	// Get custom whatsappmessage to be shared entered by user
	var customtxt = jQuery(this).attr('data-customtxt');

	var customtxt = customtxt.replace("${title}", title);
	var customtxt = customtxt.replace("${link}", link);
	var customtxt = customtxt.replace(/['"]+/g, '');// Remove single & double quotes

	var whats_app_message = title+" - "+link;
	var whatsapp_url = "whatsapp://send?text="+customtxt;
	jQuery(this).attr('href',whatsapp_url);
});	