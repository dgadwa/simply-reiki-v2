<?php
class sfsiCumulativeCount
{
   private $urlHttp;
   private $urlHttps;
   private $access_token;

   public function __construct($urlHttp,$urlHttps,$access_token='') {
        $this->urlHttp      = $urlHttp;
        $this->urlHttps     = $urlHttps;
        $this->access_token = $access_token;        
    }

    public function sfsi_get_multi_curl($data, $options = array(),$outputTypeJson=false) {
     
          // array of curl handles
          $curly = array();
          // data to be returned
          $result = array();
         
          // multi handle
          $mh = curl_multi_init();
         
          // loop through $data and create curl handles
          // then add them to the multi-handle
          foreach ($data as $id => $d) {
         
            $curly[$id] = curl_init();
         
            $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
            curl_setopt($curly[$id], CURLOPT_URL,            $url);
            curl_setopt($curly[$id], CURLOPT_HEADER,         0);
            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
         
            // post?
            if (is_array($d)) {
              if (!empty($d['post'])) {
                curl_setopt($curly[$id], CURLOPT_POST,       1);
                curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
              }
            }
         
            // extra options?
            if (!empty($options)) {
              curl_setopt_array($curly[$id], $options);
            }
         
            curl_multi_add_handle($mh, $curly[$id]);
          }
         
          // execute the handles
          $running = null;
          do {
            curl_multi_exec($mh, $running);
          } while($running > 0);
         
         
          // get content and remove handles
          foreach($curly as $id => $c) {
            $result[$id] = ($outputTypeJson) ? curl_multi_getcontent($c) : json_decode(curl_multi_getcontent($c));
            curl_multi_remove_handle($mh, $c);
          }
         
          // all done
          curl_multi_close($mh);
     
      return $result;
    }

    /********************************** Methods to get facebook cumulatitve count STARTS **************************/

    private function sfsi_fb_cumulative_api(){

        $fbUrlArr = array(
            'https://graph.facebook.com/?id='.$this->urlHttp,
            'https://graph.facebook.com/?id='.$this->urlHttps
        );
        
        $arrFbData = $this->sfsi_get_multi_curl($fbUrlArr); 
        return $arrFbData;        
    }     

    private function sfsi_fb_cumulative_api_version_27(){

        $fbUrlArr = array(
            'https://graph.facebook.com/v2.7/?id='.$this->urlHttp.'&fields=engagement&access_token='.$this->access_token,          
            'https://graph.facebook.com/v2.7/?id='.$this->urlHttps.'&fields=engagement&access_token='.$this->access_token
        );
        
        $arrFbData = $this->sfsi_get_multi_curl($fbUrlArr); 

        return $arrFbData;        
    }

    private function sfsi_fb_cumulative_api_version_29(){

        $fbUrlArr = array(
            'https://graph.facebook.com/v2.9/?id='.$this->urlHttp.'&fields=engagement&access_token='.$this->access_token,          
            'https://graph.facebook.com/v2.9/?id='.$this->urlHttps.'&fields=engagement&access_token='.$this->access_token
        );

        $arrFbData = $this->sfsi_get_multi_curl($fbUrlArr); 

        return $arrFbData;
    }

    private function sfsi_get_fb_count_api_data(){

        $arrResp = array();

        if(empty($this->access_token)){

          if($this->sfsi_check_share_key_exists($this->sfsi_fb_cumulative_api())){
              return $arrResp = array(
                        "api"=>"url",
                        "data"=> $this->sfsi_fb_cumulative_api()
              );
          }

        }
        else{

            if($this->sfsi_check_share_key_exists($this->sfsi_fb_cumulative_api_version_27())){
                return $arrResp = array(
                          "api"=>"app27",
                          "data"=> $this->sfsi_fb_cumulative_api_version_27()
                );
            }
            else if($this->sfsi_check_share_key_exists($this->sfsi_fb_cumulative_api_version_29())){
                return $arrResp = array(
                          "api"=>"app29",
                          "data"=> $this->sfsi_fb_cumulative_api_version_29()
                );
            }           
        }

        return $arrResp; 
    }

    private function sfsi_check_share_key_exists($arrFbData){

        $check = false;

        if(!empty($arrFbData)){

            foreach ($arrFbData as $fbData) {
                
                if($fbData->share->share_count || $fbData->engagement->share_count){
                    $check = true;
                    break;
                }
            }
        }

        return $check;
    }

    public function sfsi_count_cumulative(){

        $count = 0;
        
        $arrFbData = $this->sfsi_get_fb_count_api_data();

        if(!empty($arrFbData)){

            $httpData = $arrFbData['data'][0];
            $httpsData= $arrFbData['data'][1];

            if($arrFbData['api']=="url"){

              if(isset($httpsData->share->share_count) && isset($httpsData->og_object->id) && isset($httpData->share->share_count) && isset($httpData->og_object->id)){

                  if($httpsData->share->share_count==$httpData->share->share_count && $httpsData->og_object->id==$httpData->og_object->id){
                      $count = $httpsData->share->share_count;
                  }
                  else if($httpsData->share->share_count!=$httpData->share->share_count && $httpsData->og_object->id!=$httpData->og_object->id){
                      $count = $httpsData->share->share_count + $httpData->share->share_count;
                  }
                  else if($httpsData->share->share_count!=$httpData->share->share_count && $httpsData->og_object->id==$httpData->og_object->id){

                       $count =($httpsData->share->share_count > $httpData->share->share_count) ? $httpsData->share->share_count : $httpData->share->share_count;
                  }                
              }               
          }
          else{                
                if(isset($httpData->engagement->reaction_count) 
                    && isset($httpData->engagement->comment_count)
                    && isset($httpData->engagement->share_count) 
                    && isset($httpData->engagement->comment_plugin_count)

                    && isset($httpsData->engagement->reaction_count)
                    && isset($httpsData->engagement->comment_count)
                    && isset($httpsData->engagement->share_count)
                    && isset($httpsData->engagement->comment_plugin_count)
                  ){
                  
                    $httpsCount = $httpsData->engagement->reaction_count + $httpsData->engagement->comment_count + $httpsData->engagement->share_count + $httpsData->engagement->comment_plugin_count; 

                    $httpCount = $httpData->engagement->reaction_count + $httpData->engagement->comment_count + $httpData->engagement->share_count + $httpData->engagement->comment_plugin_count; 
                    
                    $count = ($httpCount==$httpsCount) ? $httpsCount :(($httpCount>$httpsCount) ? $httpCount: $httpsCount);
                }              
            }          
        }
        return $count;
    }

    /********************************** Methods to get facebook cumulatitve count CLOSES **********************/

    public function sfsi_pinterest_get_count(){

        $arrRespData = array();
        $count = 0;

        $apiUrlArr = array(
            'http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url='.$this->urlHttp,
            'http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url='.$this->urlHttps
        );

        $arrRespData = $this->sfsi_get_multi_curl($apiUrlArr,array(),true); 

        if(count($arrRespData)>0){

            foreach ($arrRespData as $respJson) {
                  $json_string = preg_replace('/^receiveCount\((.*)\)$/', "\\1", $respJson);
                  $json        = json_decode($json_string, true);
                  $count       = $count + (isset($json['count'])? intval($json['count']):0);
            }
        }
        return $count;
    }   

}
?>