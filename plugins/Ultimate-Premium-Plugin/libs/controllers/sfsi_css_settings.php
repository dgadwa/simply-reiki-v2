<?php
function sfsi_set_social_icons_desktop_mobile_css_settings_from_question3(){ ?>
           
           <?php
				//Don't show on desktop
				if(isset($option8['sfsi_plus_show_on_desktop']) && isset($option8['sfsi_plus_show_on_mobile']) && $option8['sfsi_plus_show_on_desktop'] != 'yes' && $option8['sfsi_plus_show_on_mobile'] != 'yes')
				{
					?>
					.widget.sfsi_plus, #sfsi_plus_floater, .sfsibeforpstwpr, .sfsiaftrpstwpr
					{
						display: none;
					}
					<?php
				}
				elseif(isset($option8['sfsi_plus_show_on_desktop']) && $option8['sfsi_plus_show_on_desktop'] != 'yes')
				{
					?>
					.widget.sfsi_plus, #sfsi_plus_floater, .sfsibeforpstwpr, .sfsiaftrpstwpr
					{
						display: none;
					}
					@media (max-width: 767px)
					{
						.widget.sfsi_plus, #sfsi_plus_floater, .sfsibeforpstwpr, .sfsiaftrpstwpr
						{
							display: block;
						}
					}
					<?php
				}
				elseif(isset($option8['sfsi_plus_show_on_mobile']) && $option8['sfsi_plus_show_on_mobile'] != 'yes')
				{
					?>
					@media (max-width: 767px)
					{
						.widget.sfsi_plus, #sfsi_plus_floater, .sfsibeforpstwpr, .sfsiaftrpstwpr
						{
							display: none;
						}
					}
					<?php
				}
			?>
<?php }

add_action("wp_head", "sfsi_plus_addStyleFunction");
function sfsi_plus_addStyleFunction()
{
	if(sfsi_is_icons_showing_on_front()){
		
		$option9 			= unserialize(get_option('sfsi_premium_section9_options',false));
		$option8 			= unserialize(get_option('sfsi_premium_section8_options',false));
		$option5 			= unserialize(get_option('sfsi_premium_section5_options',false));

		$sfsi_plus_feediid 	= sanitize_text_field(get_option('sfsi_premium_feed_id'));
		$url 				= "https://www.specificfeeds.com/widgets/subscribeWidget/";
		echo $return = '';

	 ?>
    	<script>
			jQuery(document).ready(function(e) {
                jQuery("body").addClass("sfsi_plus_<?php echo get_option("sfsi_premium_pluginVersion");?>")
            });
			function sfsi_plus_processfurther(ref) {
				var feed_id = '<?php echo $sfsi_plus_feediid?>';
				var feedtype = 8;
				var email = jQuery(ref).find('input[name="data[Widget][email]"]').val();
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if ((email != "Enter your email") && (filter.test(email))) {
					if (feedtype == "8") {
						var url = "<?php echo $url; ?>"+feed_id+"/"+feedtype;
						window.open(url, "popupwindow", "scrollbars=yes,width=1080,height=760");
						return true;
					}
				} else {
					alert("Please enter email address");
					jQuery(ref).find('input[name="data[Widget][email]"]').focus();
					return false;
				}
			}
		</script>
    	<style type="text/css">

    	    <?php

				if(isset($option8['sfsi_plus_shortcode_horizontal_verical_Alignment']) && $option8['sfsi_plus_shortcode_horizontal_verical_Alignment'] == "Vertical"){
					
		    		$icons_space 		 = $option5['sfsi_plus_icons_spacing'];
		    		$icons_size 		 = $option5['sfsi_plus_icons_size'];

		    		$shortcodeContainerWidth = (int) $icons_size + (int) $icons_space; 
					
					if(isset($shortcodeContainerWidth)){ ?>

						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv{
							width:<?php echo $shortcodeContainerWidth; ?>px !important;
						}
						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv div:nth-child(1){
							margin-left: <?php echo isset($icons_space) ? $icons_space : 5; ?>px !important;	
						}			

					<?php }
				}
    	    
    	    	if(false == wp_is_mobile()){

					if($option5['sfsi_plus_horizontal_verical_Alignment']=="Horizontal" && isset($option5['sfsi_plus_icons_spacing'])){ ?>
						
						#sfsi_plus_floater .sfsi_plus_wicons:nth-child(1){
							margin-left: 5px !important;
						}

					<?php } 
    	    	}

				if($option5['sfsi_plus_mobile_horizontal_verical_Alignment']=="Horizontal"){ ?>
					
					.sfsi_plus_mobile_floater .sfsi_plus_wicons{
						display: inline-block !important;
					}

				<?php } 				

            	if($option5['sfsi_plus_tooltip_alighn'] == 'down')
		       	{
                  ?>
                   .sfsi_plus_Tlleft{top: 95%;left: 50%;}
                   .sfsi_plus_tool_tip_2 .bot_arow{ top: -5px; }
				 <?php 
				} 
			?>
			<?php 
            
            if(wp_is_mobile() && $option5['sfsi_plus_mobile_icon_alignment_setting'] == 'yes')
            {
            	if($option5['sfsi_plus_mobile_icons_Alignment_via_widget'] == 'left')
            	{
            	    ?>	
                    	.sfsi_plus.sfsi_plus_widget_main_container
						{
							text-align: left;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_widget_sub_container
						{
							float: left;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_holders
						{
							display: none;
						}
                    <?php
            	}
            	elseif ($option5['sfsi_plus_mobile_icons_Alignment_via_widget'] == 'right') 
            	{
            		?>  
            		   .sfsi_plus.sfsi_plus_widget_main_container
						{
							text-align: right;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_widget_sub_container
						{
							float: right;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
            	 }	
            	 elseif ($option5['sfsi_plus_mobile_icons_Alignment_via_widget'] == 'center')
            	 {
            	 	 ?>
                        .sfsi_plus.sfsi_plus_widget_main_container
						{
							text-align: center;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
							float: none;
							margin: 0 auto;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_holders
						{
							display: none;
						}

            	 	 <?php
            	 }
            	 if($option5['sfsi_plus_mobile_icons_Alignment_via_shortcode'] == 'left')
            	 {
            	 	?>
            	 	.sfsi_plus_shortcode_container
						{
							float: left;
						}
						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus_shortcode_container .sfsi_plus_holders
						{
							display: none;
						}

            	 	<?php
            	 }
            	 elseif ($option5['sfsi_plus_mobile_icons_Alignment_via_shortcode'] == 'right')
            	 {
            	 	?>
                       .sfsi_plus_shortcode_container
						{
							float: right;
						}
						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus_shortcode_container .sfsi_plus_holders
						{
							display: none;
						}
            	 	<?php
            	 }
            	 elseif ($option5['sfsi_plus_mobile_icons_Alignment_via_shortcode'] == 'center')
            	 {
            	    ?>
            	    .sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
							float: none;
							margin: 0 auto;
						}
						.sfsi_plus_shortcode_container .sfsi_plus_holders
						{
							display: none;
						}
            	    <?php
            	 }
            }
			else
			{
				if($option5['sfsi_plus_icons_Alignment_via_widget'] == 'left')
				{
					?>
						.sfsi_plus.sfsi_plus_widget_main_container
						{
							text-align: left;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_widget_sub_container
						{
							float: left;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
				}
				elseif($option5['sfsi_plus_icons_Alignment_via_widget'] == 'right')
				{
					?>
						.sfsi_plus.sfsi_plus_widget_main_container
						{
							text-align: right;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_widget_sub_container
						{
							float: right;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
				}
				elseif($option5['sfsi_plus_icons_Alignment_via_widget'] == 'center')
				{
					?>
						.sfsi_plus.sfsi_plus_widget_main_container
						{
							text-align: center;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
							float: none;
							margin: 0 auto;
						}
						.sfsi_plus.sfsi_plus_widget_main_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
				}

				if($option5['sfsi_plus_icons_Alignment_via_shortcode'] == 'left')
				{
					?>
						.sfsi_plus_shortcode_container
						{
							float: left;
						}
						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus_shortcode_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
				}
				elseif($option5['sfsi_plus_icons_Alignment_via_shortcode'] == 'right')
				{
					?>
						.sfsi_plus_shortcode_container
						{
							float: right;
						}
						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
						}
						.sfsi_plus_shortcode_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
				}
				elseif($option5['sfsi_plus_icons_Alignment_via_shortcode'] == 'center')
				{
					?>
						.sfsi_plus_shortcode_container .sfsiplus_norm_row.sfsi_plus_wDiv
						{
							position: relative !important;
							float: none;
							margin: 0 auto;
						}
						.sfsi_plus_shortcode_container .sfsi_plus_holders
						{
							display: none;
						}
					<?php
				}
			}
			?>
			.sfsiaftrpstwpr .sfsi_plus_Sicons div:first-child span, .sfsibeforpstwpr .sfsi_plus_Sicons div:first-child span
			{
				font-size: <?php echo ($option8['sfsi_plus_textBefor_icons_font_size']!=0) ? $option8['sfsi_plus_textBefor_icons_font_size']:20; ?>px;
				font-style: <?php echo $option8['sfsi_plus_textBefor_icons_font_type']; ?>;
				font-family: <?php echo $option8['sfsi_plus_textBefor_icons_font']; ?>;
				color: <?php echo $option8['sfsi_plus_textBefor_icons_fontcolor']; ?>;
			}
			
			.sfsibeforpstwpr, .sfsiaftrpstwpr {
				margin-top: <?php echo (!empty($option8['sfsi_plus_marginAbove_postIcon'])) ? $option8['sfsi_plus_marginAbove_postIcon'] : "5";?>px !important;
				margin-bottom: <?php echo (!empty($option8['sfsi_plus_marginBelow_postIcon'])) ? $option8['sfsi_plus_marginBelow_postIcon'] : "5";?>px !important;
			}

			.sfsi_plus_rectangle_icons_shortcode_container {
				margin-top: <?php echo (!empty($option8['sfsi_plus_marginAbove_postIcon'])) ? $option8['sfsi_plus_marginAbove_postIcon'] : "5";?>px !important;
				margin-bottom: <?php echo (!empty($option8['sfsi_plus_marginBelow_postIcon'])) ? $option8['sfsi_plus_marginBelow_postIcon'] : "5";?>px !important;
			}

			.sfsi_plus_subscribe_Popinner
			{
				<?php if($option9['sfsi_plus_form_adjustment'] == 'yes') : ?>
				width: 100% !important;
				height: auto !important;
				<?php else: ?>
				width: <?php echo $option9['sfsi_plus_form_width'] ?>px !important;
				height: <?php echo $option9['sfsi_plus_form_height'] ?>px !important;
				<?php endif;?>
				<?php if($option9['sfsi_plus_form_border'] == 'yes') : ?>
				border: <?php echo $option9['sfsi_plus_form_border_thickness']."px solid ".$option9['sfsi_plus_form_border_color'];?> !important;
				<?php endif;?>
				padding: 18px 0px !important;
				background-color: <?php echo $option9['sfsi_plus_form_background'] ?> !important;
			}
			.sfsi_plus_subscribe_Popinner form
			{
				margin: 0 20px !important;
			}
			.sfsi_plus_subscribe_Popinner h5
			{
				font-family: <?php echo $option9['sfsi_plus_form_heading_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_heading_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_heading_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_heading_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_heading_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_heading_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_heading_fontalign'] ?> !important;
				margin: 0 0 10px !important;
    			padding: 0 !important;
			}
			.sfsi_plus_subscription_form_field {
				margin: 5px 0 !important;
				width: 100% !important;
				display: inline-flex;
				display: -webkit-inline-flex;
			}
			.sfsi_plus_subscription_form_field input {
				width: 100% !important;
				padding: 10px 0px !important;
			}
			.sfsi_plus_subscribe_Popinner input[type=email]
			{
				font-family: <?php echo $option9['sfsi_plus_form_field_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_field_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_field_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_field_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_field_fontalign'] ?> !important;
			}
			.sfsi_plus_subscribe_Popinner input[type=email]::-webkit-input-placeholder {
			   font-family: <?php echo $option9['sfsi_plus_form_field_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_field_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_field_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_field_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_field_fontalign'] ?> !important;
			}
			
			.sfsi_plus_subscribe_Popinner input[type=email]:-moz-placeholder { /* Firefox 18- */
			    font-family: <?php echo $option9['sfsi_plus_form_field_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_field_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_field_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_field_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_field_fontalign'] ?> !important;
			}
			
			.sfsi_plus_subscribe_Popinner input[type=email]::-moz-placeholder {  /* Firefox 19+ */
			    font-family: <?php echo $option9['sfsi_plus_form_field_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_field_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_field_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_field_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_field_fontalign'] ?> !important;
			}
			
			.sfsi_plus_subscribe_Popinner input[type=email]:-ms-input-placeholder {  
			  	font-family: <?php echo $option9['sfsi_plus_form_field_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_field_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_field_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_field_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_field_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_field_fontalign'] ?> !important;
			}
			.sfsi_plus_subscribe_Popinner input[type=submit]
			{
				font-family: <?php echo $option9['sfsi_plus_form_button_font'] ?> !important;
				<?php if($option9['sfsi_plus_form_button_fontstyle'] != 'bold') {?>
				font-style: <?php echo $option9['sfsi_plus_form_button_fontstyle'] ?> !important;
				<?php } else{ ?>
				font-weight: <?php echo $option9['sfsi_plus_form_button_fontstyle'] ?> !important;
				<?php }?>
				color: <?php echo $option9['sfsi_plus_form_button_fontcolor'] ?> !important;
				font-size: <?php echo $option9['sfsi_plus_form_button_fontsize']."px" ?> !important;
				text-align: <?php echo $option9['sfsi_plus_form_button_fontalign'] ?> !important;
				background-color: <?php echo $option9['sfsi_plus_form_button_background'] ?> !important;
			}
		</style>
	<?php }
}

function sfsi_plus_custom_css_from_Que6(){
	
	if(sfsi_is_icons_showing_on_front()){

		$option5   = unserialize(get_option('sfsi_premium_section5_options',false));
		$customCss = (isset($option5['sfsi_plus_custom_css'])) ? unserialize($option5['sfsi_plus_custom_css']): '';
	    $customCss = wp_kses( $customCss, array( '\'', '\"' ) );
	    $customCss = str_replace( '&gt;', '>', $customCss );
	    ob_start();
	    ?>
	    <style type="text/css"><?php echo $customCss;?></style>   
		<?php 
		echo ob_get_clean();
	}
}
add_action('wp_head', 'sfsi_plus_custom_css_from_Que6');
?>