<?php 
 /* show a pop on the as per user chose under section 7 */
function sfsi_plus_frontPopUp ()
{ 
	if(sfsi_is_icons_showing_on_front()){
		
		$sfsi_premium_section7_options 	= 	unserialize(get_option('sfsi_premium_section7_options',false));
     
		if (wp_is_mobile())
		{
			if(isset($sfsi_premium_section7_options['sfsi_plus_popup_show_on_mobile']) && $sfsi_premium_section7_options['sfsi_plus_popup_show_on_mobile'] == 'yes')
			{
				ob_start();
				echo sfsi_plus_FrontPopupDiv();
				echo  $output=ob_get_clean();
			}
		}
		else
		{
			if(isset($sfsi_premium_section7_options['sfsi_plus_popup_show_on_desktop']) && $sfsi_premium_section7_options['sfsi_plus_popup_show_on_desktop'] == 'yes')
			{
				ob_start();
				echo sfsi_plus_FrontPopupDiv();
				echo  $output=ob_get_clean();
			}
		}     
	}
}
/* check where to be pop-shown */
function sfsi_plus_check_PopUp($content)
{
	if(sfsi_is_icons_showing_on_front()){

		global $post; global $wpdb;
		
		$sfsi_premium_section7_options 	= 	unserialize(get_option('sfsi_premium_section7_options',false));
		$sfsi_plus_popup_limit_count	= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_limit_count']))
		 										? $sfsi_premium_section7_options['sfsi_plus_popup_limit_count']
												: 1;
		$sfsi_plus_popup_limit_type		=  (
												isset($sfsi_premium_section7_options['sfsi_plus_popup_limit_type']) &&
												$sfsi_premium_section7_options['sfsi_plus_popup_limit_type'] == "day"
											)
		 										? (24*3600)
												: (3600);
												
		// Get pop up cookie expiration Time
		$popTime = (int)$sfsi_plus_popup_limit_count	* (int)$sfsi_plus_popup_limit_type;
									
		if($sfsi_premium_section7_options['sfsi_plus_Show_popupOn']=="blogpage")
	    {   
		   	if(!is_feed() && !is_home() && !is_page())
			{
			     $content=  sfsi_plus_frontPopUp ().$content;
		     }
	    }
		else if($sfsi_premium_section7_options['sfsi_plus_Show_popupOn']=="selectedpage")
	    {
		 	if(!empty($post->ID) && !empty($sfsi_premium_section7_options['sfsi_plus_Show_popupOn_PageIDs']))
			{
				if(is_page() && in_array($post->ID,  unserialize($sfsi_premium_section7_options['sfsi_plus_Show_popupOn_PageIDs'])))
				{
					 $content=  sfsi_plus_frontPopUp ().$content;
				}
			}
	    }
	    else if($sfsi_premium_section7_options['sfsi_plus_Show_popupOn']=="everypage")
		{
		 	$content= sfsi_plus_frontPopUp ().$content;
	    }
		
	?>	
	<script type="text/javascript">
		jQuery(document).ready(function(e) {
           <?php if($sfsi_premium_section7_options['sfsi_plus_popup_limit'] == "no") { ?>sfsi_plus_eraseCookie("sfsi_popup");<?php } ?>
        });
		
		function sfsi_plus_hidemypopup()
		{
			SFSI(".sfsi_plus_FrntInner").fadeOut("fast");
			<?php if(in_array("leavePage", $sfsi_premium_section7_options['sfsi_plus_Shown_pop'])) { ?>popUpOnLeavePage = false;<?php } ?>
			<?php if($sfsi_premium_section7_options['sfsi_plus_popup_limit'] == "yes") { ?>
				sfsi_plus_setCookie("sfsi_popup","yes",<?php echo $popTime; ?>);
			<?php } ?>
		}
    </script>
   	<?php

	    /* check for pop times */
	    if(in_array("once", $sfsi_premium_section7_options['sfsi_plus_Shown_pop']))
	    {
			$time_popUp = $sfsi_premium_section7_options['sfsi_plus_Shown_popupOnceTime'];
			$time_popUp = $time_popUp*1000;
		 	ob_start();
			?>
	        <script>
				jQuery( document ).ready(function( $ )
				{
					var cookieVal = sfsi_plus_getCookie("sfsi_popup");
					if(typeof cookieVal !== "string")
					{
						setTimeout(
							function()
							{	
								jQuery('.sfsi_plus_outr_div').css({'z-index':'1000000',opacity:1});
								jQuery('.sfsi_plus_outr_div').fadeIn();
								jQuery('.sfsi_plus_FrntInner').fadeIn(200);
							},<?php echo $time_popUp; ?>
						);
					}
				});
	        </script>
	        <?php 
	     	echo ob_get_clean();
	    }
		 
	    if(in_array("ETscroll", $sfsi_premium_section7_options['sfsi_plus_Shown_pop']))
	    {
			$time_popUp = $sfsi_premium_section7_options['sfsi_plus_Shown_popupOnceTime'];
			$time_popUp = (int)$time_popUp*1000;
		    ob_start();
			?>
	     	<script>
		    jQuery( document ).scroll(function( $ )
			{
				var cookieVal = sfsi_plus_getCookie("sfsi_popup");
				if(typeof cookieVal !== "string")
				{
					var y = jQuery(this).scrollTop();
					if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
					{	 
						if(jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height()-100)
						{
						  jQuery('.sfsi_plus_outr_div').css({'z-index':'9996',opacity:1,top:jQuery(window).scrollTop()+"px",position:"absolute"});
						  jQuery('.sfsi_plus_outr_div').fadeIn(200);
						  jQuery('.sfsi_plus_FrntInner').fadeIn(200);
						}
						else
						{
						   jQuery('.sfsi_plus_outr_div').fadeOut();
						   jQuery('.sfsi_plus_FrntInner').fadeOut();
						}
					}
					else
					{
						if(jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height()-3)
						{
							jQuery('.sfsi_plus_outr_div').css({'z-index':'9996',opacity:1,top:jQuery(window).scrollTop()+200+"px",position:"absolute"});
							jQuery('.sfsi_plus_outr_div').fadeIn(200);
							jQuery('.sfsi_plus_FrntInner').fadeIn(200);
						}
						else
						{
							jQuery('.sfsi_plus_outr_div').fadeOut();
							jQuery('.sfsi_plus_FrntInner').fadeOut();
						}
					}
				}
			});
	    </script>
	    <?php 
	     	echo ob_get_clean();
	    }
		 
		if(in_array("leavePage", $sfsi_premium_section7_options['sfsi_plus_Shown_pop']))
		{
			ob_start();
				?>
				<script>
					var popUpOnLeavePage = true;
					function addEvent(obj, evt, fn) {
						if (obj.addEventListener) {
							obj.addEventListener(evt, fn, false);
						}
						else if (obj.attachEvent) {
							obj.attachEvent("on" + evt, fn);
						}
					}
					addEvent(document, 'mouseout', function(evt) {
						var cookieVal = sfsi_plus_getCookie("sfsi_popup");
						if(typeof cookieVal !== "string")
						{
							if(popUpOnLeavePage)
							{
								if (evt.toElement == null && evt.relatedTarget == null ) {
									jQuery('.sfsi_plus_outr_div').css({
										'z-index':'9996',
										opacity:1,
										top:jQuery(window).scrollTop()+200+"px",
										position:"fixed"
									});
									jQuery('.sfsi_plus_outr_div').fadeIn(200);
									jQuery('.sfsi_plus_FrntInner').fadeIn(200);
								};
							}
						}
					});
					var SFSI = jQuery.noConflict();
				</script>
			<?php
			echo ob_get_clean();
	    }
		
		if(in_array("LimitPopUp", $sfsi_premium_section7_options['sfsi_plus_Shown_pop']))
	    {
		 	$time_popUp = $sfsi_premium_section7_options['sfsi_plus_Shown_popuplimitPerUserTime'];
		 	$end_time = $_COOKIE['sfsi_socialPopUp']+($time_popUp*60); 
			$time_popUp = $time_popUp*1000;
	     
		 	if(!empty($end_time))
			{
				 if($end_time<time())
				 {     
				 	ob_start();
					?>
				 <script>
					jQuery( document ).ready(function( $ ) {
					    //SFSI('.sfsi_plus_outr_div').fadeIn();
				  		sfsi_plus_setCookie('sfsi_socialPopUp',<?php echo time(); ?>,32);
				  		setTimeout(function() {jQuery('.sfsi_plus_outr_div').css({'z-index':'1000000',opacity:1});jQuery('.sfsi_plus_outr_div').fadeIn();},<?php echo $time_popUp; ?>);
					});
					var SFSI = jQuery.noConflict();
				</script>
				<?php
				}
			 }
	     	echo ob_get_clean();
	    } 

	}	   
	return $content;
}

/* make front end pop div */
function sfsi_plus_FrontPopupDiv()
{
	$icons = "";
	$sfsi_premium_section7_options = unserialize(get_option('sfsi_premium_section7_options',false));
    $sfsi_section4 				   = unserialize(get_option('sfsi_premium_section4_options',false)); 
            
	//************************************** Get settings for popup STARTS ****************************************//
    
    /* calculate the width and icons display alignments */
    $heading_text 		=	(isset($sfsi_premium_section7_options['sfsi_plus_popup_text']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_text']
								: 'Enjoy this site? Please follow and like us!';
								
    $div_bgColor		= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_background_color']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_background_color']
								: '#fff';
								
    $div_FontFamily 	= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_font']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_font']
								: 'Arial';
    $div_BorderColor	= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_border_color']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_border_color']
								: '#d3d3d3';
    $div_Fonttyle		= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_fontStyle']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_fontStyle']
								: 'normal';
    $div_FontColor		= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_fontColor']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_fontColor']
								: '#000';
    $div_FontSize		= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_fontSize']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_fontSize']
								: '26';
    $div_BorderTheekness= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_border_thickness']))
								? $sfsi_premium_section7_options['sfsi_plus_popup_border_thickness']
								: '1';
    $div_Shadow			= 	(isset($sfsi_premium_section7_options['sfsi_plus_popup_border_shadow']) &&
							$sfsi_premium_section7_options['sfsi_plus_popup_border_shadow']=="yes")
								? $sfsi_premium_section7_options['sfsi_plus_popup_border_thickness']
								: 'no'; 
    
    $style = "background-color:".$div_bgColor.";border:".$div_BorderTheekness."px solid".$div_BorderColor."; font-style:".$div_Fonttyle.";color:".$div_FontColor;
    if($sfsi_premium_section7_options['sfsi_plus_popup_border_shadow']=="yes")
    {
       $style.=";box-shadow:12px 30px 18px #CCCCCC;";
    }    
    
	$h_style="font-family:".$div_FontFamily.";font-style:".$div_Fonttyle.";color:".$div_FontColor.";font-size:".$div_FontSize."px";
    
	//************************************* Get settings for popup CLOSES ****************************************//

   
	$closeFunction = "sfsi_plus_hidemypopup();";

	//************************************* GENERATING FINAL HTML FOR POPUP starts ****************************************//

	$icons = '<div class="sfsi_plus_outr_div" > <div class="sfsi_plus_FrntInner" style="'.$style.'">';
	$icons.='<div class="sfsiclpupwpr" onclick="'.$closeFunction.'"><img src="'.SFSI_PLUS_PLUGURL.'images/close.png" alt="close" /></div>';
	
	if(!empty($heading_text))
	{
		$icons.='<h2 style="'.$h_style.'">'.$heading_text.'</h2>';
	}
    
	$ulmargin="";
    if($sfsi_section4['sfsi_plus_display_counts']=="no")
    {
		$ulmargin="margin-bottom:0px";
    }

    if(isset($sfsi_premium_section7_options['sfsi_plus_popup_type_iconsOrForm']) && $sfsi_premium_section7_options['sfsi_plus_popup_type_iconsOrForm']=="form"){
    	$icons.= sfsi_plus_get_subscriberForm($ulmargin);    		
    }
    else{
    	$icons.= sfsi_plus_icons_html($ulmargin);    		    	
    }

	$icons.='</div></div>';

	//*************************** GENERATING FINAL HTML FOR POPUP closes ****************************************/    
    return $icons;
}

function sfsi_plus_icons_html($ulmargin){
   
   $icons='';
   $sfsi_premium_section1_options = unserialize(get_option('sfsi_premium_section1_options',false));
   $sfsi_premium_section8_options = unserialize(get_option('sfsi_premium_section8_options',false));   
   $sfsi_premium_section7_options = unserialize(get_option('sfsi_premium_section7_options',false));

   $sfsi_section5 				  = unserialize(get_option('sfsi_premium_section5_options',false));  
   $custom_i 					  = unserialize($sfsi_premium_section1_options['sfsi_custom_files']);
    
	if($sfsi_premium_section1_options['sfsi_plus_rss_display']=='no' &&  $sfsi_premium_section1_options['sfsi_plus_email_display']=='no' && $sfsi_premium_section1_options['sfsi_plus_facebook_display']=='no' && $sfsi_premium_section1_options['sfsi_plus_twitter_display']=='no' &&  $sfsi_premium_section1_options['sfsi_plus_google_display']=='no' && $sfsi_premium_section1_options['sfsi_plus_share_display']=='no' && $sfsi_premium_section1_options['sfsi_plus_youtube_display']=='no' && $sfsi_premium_section1_options['sfsi_plus_pinterest_display']=='no' && $sfsi_premium_section1_options['sfsi_plus_linkedin_display']=='no' && empty($custom_i)) 
    {     	
		return $icons;
		exit;
    }

    /* get all icons including custom icons */
    $custom_icons_order = unserialize($sfsi_section5['sfsi_plus_CustomIcons_order']);
    $icons_order = array(
		$sfsi_section5['sfsi_plus_rssIcon_order']	=>'rss',
		$sfsi_section5['sfsi_plus_emailIcon_order']	=>'email',
		$sfsi_section5['sfsi_plus_facebookIcon_order']	=>'facebook',
		$sfsi_section5['sfsi_plus_googleIcon_order']	=>'google',
		$sfsi_section5['sfsi_plus_twitterIcon_order']	=>'twitter',
		$sfsi_section5['sfsi_plus_shareIcon_order']	=>'share',
		$sfsi_section5['sfsi_plus_youtubeIcon_order']	=>'youtube',
		$sfsi_section5['sfsi_plus_pinterestIcon_order']=>'pinterest',
		$sfsi_section5['sfsi_plus_linkedinIcon_order']	=>'linkedin',
		$sfsi_section5['sfsi_plus_instagramIcon_order']=>'instagram',
		(isset($sfsi_section5['sfsi_plus_houzzIcon_order']))
			? $sfsi_section5['sfsi_plus_houzzIcon_order']
			: 11 => 'houzz',
		(isset($sfsi_section5['sfsi_plus_snapchatIcon_order']))
			? $sfsi_section5['sfsi_plus_snapchatIcon_order']
			: 12 => 'snapchat',
		(isset($sfsi_section5['sfsi_plus_whatsappIcon_order']))
			? $sfsi_section5['sfsi_plus_whatsappIcon_order']
			: 13 => 'whatsapp',
		(isset($sfsi_section5['sfsi_plus_skypeIcon_order']))
			? $sfsi_section5['sfsi_plus_skypeIcon_order']
			: 14 => 'skype',
		(isset($sfsi_section5['sfsi_plus_vimeoIcon_order']))
			? $sfsi_section5['sfsi_plus_vimeoIcon_order']
			: 15 => 'vimeo',
		(isset($sfsi_section5['sfsi_plus_soundcloudIcon_order']))
			? $sfsi_section5['sfsi_plus_soundcloudIcon_order']
			: 16 => 'soundcloud',
		(isset($sfsi_section5['sfsi_plus_yummlyIcon_order']))
			? $sfsi_section5['sfsi_plus_yummlyIcon_order']
			: 17 => 'yummly',
		(isset($sfsi_section5['sfsi_plus_flickrIcon_order']))
			? $sfsi_section5['sfsi_plus_flickrIcon_order']
			: 18 => 'flickr',
		(isset($sfsi_section5['sfsi_plus_redditIcon_order']))
			? $sfsi_section5['sfsi_plus_redditIcon_order']
			: 19 => 'reddit',
		(isset($sfsi_section5['sfsi_plus_tumblrIcon_order']))
			? $sfsi_section5['sfsi_plus_tumblrIcon_order']
			: 20 => 'tumblr'
	) ;
  
  	$arrIcons = array();
  	$elements = array();
  	$arrIcons = unserialize($sfsi_premium_section1_options['sfsi_custom_files']);
  	if(is_array($icons))  $elements = array_keys($arrIcons);
  
	$cnt=0;
	$total=count($custom_icons_order);
	if(!empty($arrIcons) && is_array($arrIcons))
	{
		foreach($arrIcons as $cn=>$c_icons)
		{
			if(is_array($custom_icons_order) ) :
				if(in_array($custom_icons_order[$cnt]['ele'],$elements)) :   
					$key = key($elements);
					unset($elements[$key]);
					$icons_order[$custom_icons_order[$cnt]['order']]=array('ele'=>$cn,'img'=>$c_icons);
				else :
					$icons_order[]=array('ele'=>$cn,'img'=>$c_icons);
				endif;
				$cnt++;
		  	else :
				$icons_order[]=array('ele'=>$cn,'img'=>$c_icons);
		  	endif;
		}
	}
	ksort($icons_order);/* short icons in order to display */

    $icons.='<ul style="'.$ulmargin.'">';
	/* make icons with all settings saved in admin  */
    if (wp_is_mobile())
	{
		// Show on mobile yes
		if(isset($sfsi_premium_section7_options['sfsi_plus_popup_show_on_mobile']) && $sfsi_premium_section7_options['sfsi_plus_popup_show_on_mobile'] == 'yes')
		{
			// different selection for mobile then
			if($sfsi_premium_section1_options['sfsi_plus_icons_onmobile'] == 'yes')
			{
				foreach($icons_order  as $index	=>	$icn)
				{
					if(is_array($icn)) { $icon_arry=$icn; $icn="custom" ; }
					
					switch ($icn) :     
						case 'rss' :
							if($sfsi_premium_section1_options['sfsi_plus_rss_mobiledisplay']=='yes')  $icons.= "<li>".sfsi_plus_prepairIcons('rss',1)."</li>";  
						break;
						case 'email' :
							if($sfsi_premium_section1_options['sfsi_plus_email_mobiledisplay']=='yes')   $icons.= "<li>".sfsi_plus_prepairIcons('email',1)."</li>"; 
						break;
						case 'facebook' :
							if($sfsi_premium_section1_options['sfsi_plus_facebook_mobiledisplay']=='yes') $icons.= "<li>".sfsi_plus_prepairIcons('facebook',1)."</li>";
						break;
						case 'google' :
							if($sfsi_premium_section1_options['sfsi_plus_google_mobiledisplay']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('google',1)."</li>";
						break;
						case 'twitter' :
							if($sfsi_premium_section1_options['sfsi_plus_twitter_mobiledisplay']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('twitter',1)."</li>"; 
						break;
						case 'share' :
							if($sfsi_premium_section1_options['sfsi_plus_share_mobiledisplay']=='yes')    $icons.= "<li id='SFshareIcon'>".sfsi_plus_prepairIcons('share',1)."</li>";
						break;
						case 'youtube' :
							if($sfsi_premium_section1_options['sfsi_plus_youtube_mobiledisplay']=='yes')     $icons.= "<li>".sfsi_plus_prepairIcons('youtube',1)."</li>"; 
						break;
						case 'pinterest' :
							if($sfsi_premium_section1_options['sfsi_plus_pinterest_mobiledisplay']=='yes')     $icons.= "<li>".sfsi_plus_prepairIcons('pinterest',1)."</li>";
						break;
						case 'linkedin' :
							if($sfsi_premium_section1_options['sfsi_plus_linkedin_mobiledisplay']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('linkedin',1)."</li>"; 
						break;
						case 'instagram' :
							if($sfsi_premium_section1_options['sfsi_plus_instagram_mobiledisplay']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('instagram',1)."</li>"; 
						break;
						case 'houzz' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_houzz_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_houzz_mobiledisplay']=='yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('houzz',1)."</li>";
							}
						break;
						case 'snapchat' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_snapchat_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_snapchat_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('snapchat',1)."</li>";
							}
						break;
						case 'whatsapp' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_whatsapp_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_whatsapp_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('whatsapp',1)."</li>";
							}
						break;
						case 'skype' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_skype_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_skype_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('skype',1)."</li>";
							}
						break;
						case 'vimeo' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_vimeo_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_vimeo_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('vimeo',1)."</li>";
							}
						break;
						case 'soundcloud' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_soundcloud_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_soundcloud_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('soundcloud',1)."</li>";
							}
						break;
						case 'yummly' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_yummly_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_yummly_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('yummly',1)."</li>";
							}
						break;
						case 'flickr' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_flickr_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_flickr_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('flickr',1)."</li>";
							}
						break;
						case 'reddit' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_reddit_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_reddit_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('reddit',1)."</li>";
							}
						break;
						case 'tumblr' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_tumblr_mobiledisplay']) &&
								$sfsi_premium_section1_options['sfsi_plus_tumblr_mobiledisplay'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('tumblr',1)."</li>";
							}
						break;
					endswitch;
				}
			}
			// No then desktop selections loaded
			else
			{
				foreach($icons_order  as $index	=>	$icn)
				{
					if(is_array($icn)) { $icon_arry=$icn; $icn="custom" ; }
					
					switch ($icn) :     
						case 'rss' :
							if($sfsi_premium_section1_options['sfsi_plus_rss_display']=='yes')  $icons.= "<li>".sfsi_plus_prepairIcons('rss',1)."</li>";  
						break;
						case 'email' :
							if($sfsi_premium_section1_options['sfsi_plus_email_display']=='yes')   $icons.= "<li>".sfsi_plus_prepairIcons('email',1)."</li>"; 
						break;
						case 'facebook' :
							if($sfsi_premium_section1_options['sfsi_plus_facebook_display']=='yes') $icons.= "<li>".sfsi_plus_prepairIcons('facebook',1)."</li>";
						break;
						case 'google' :
							if($sfsi_premium_section1_options['sfsi_plus_google_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('google',1)."</li>";
						break;
						case 'twitter' :
							if($sfsi_premium_section1_options['sfsi_plus_twitter_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('twitter',1)."</li>"; 
						break;
						case 'share' :
							if($sfsi_premium_section1_options['sfsi_plus_share_display']=='yes')    $icons.= "<li id='SFshareIcon'>".sfsi_plus_prepairIcons('share',1)."</li>";
						break;
						case 'youtube' :
							if($sfsi_premium_section1_options['sfsi_plus_youtube_display']=='yes')     $icons.= "<li>".sfsi_plus_prepairIcons('youtube',1)."</li>"; 
						break;
						case 'pinterest' :
							if($sfsi_premium_section1_options['sfsi_plus_pinterest_display']=='yes')     $icons.= "<li>".sfsi_plus_prepairIcons('pinterest',1)."</li>";
						break;
						case 'linkedin' :
							if($sfsi_premium_section1_options['sfsi_plus_linkedin_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('linkedin',1)."</li>"; 
						break;
						case 'instagram' :
							if($sfsi_premium_section1_options['sfsi_plus_instagram_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('instagram',1)."</li>"; 
						break;
						case 'houzz' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_houzz_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_houzz_display']=='yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('houzz',1)."</li>";
							}
						break;
						case 'snapchat' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_snapchat_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_snapchat_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('snapchat',1)."</li>";
							}
						break;
						case 'whatsapp' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_whatsapp_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_whatsapp_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('whatsapp',1)."</li>";
							}
						break;
						case 'skype' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_skype_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_skype_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('skype',1)."</li>";
							}
						break;
						case 'vimeo' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_vimeo_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_vimeo_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('vimeo',1)."</li>";
							}
						break;
						case 'soundcloud' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_soundcloud_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_soundcloud_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('soundcloud',1)."</li>";
							}
						break;
						case 'yummly' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_yummly_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_yummly_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('yummly',1)."</li>";
							}
						break;
						case 'flickr' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_flickr_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_flickr_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('flickr',1)."</li>";
							}
						break;
						case 'reddit' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_reddit_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_reddit_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('reddit',1)."</li>";
							}
						break;
						case 'tumblr' :
							if(
								isset($sfsi_premium_section1_options['sfsi_plus_tumblr_display']) &&
								$sfsi_premium_section1_options['sfsi_plus_tumblr_display'] == 'yes'
							)
							{
								$icons.= "<li>".sfsi_plus_prepairIcons('tumblr',1)."</li>";
							}
						break;
						case 'custom' :
							$icons.= "<li>". sfsi_plus_prepairIcons($icon_arry['ele'],1)."</li>"; 
						break;    
					endswitch;
				}
			}
		}
	}
	else
	{
		if(isset($sfsi_premium_section7_options['sfsi_plus_popup_show_on_desktop']) && $sfsi_premium_section7_options['sfsi_plus_popup_show_on_desktop'] == 'yes')
		{
			foreach($icons_order  as $index	=>	$icn)
			{
				if(is_array($icn)) { $icon_arry=$icn; $icn="custom" ; }
				
				switch ($icn) :     
					case 'rss' :
						if($sfsi_premium_section1_options['sfsi_plus_rss_display']=='yes')  $icons.= "<li>".sfsi_plus_prepairIcons('rss',1)."</li>";  
					break;
					case 'email' :
						if($sfsi_premium_section1_options['sfsi_plus_email_display']=='yes')   $icons.= "<li>".sfsi_plus_prepairIcons('email',1)."</li>"; 
					break;
					case 'facebook' :
						if($sfsi_premium_section1_options['sfsi_plus_facebook_display']=='yes') $icons.= "<li>".sfsi_plus_prepairIcons('facebook',1)."</li>";
					break;
					case 'google' :
						if($sfsi_premium_section1_options['sfsi_plus_google_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('google',1)."</li>";
					break;
					case 'twitter' :
						if($sfsi_premium_section1_options['sfsi_plus_twitter_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('twitter',1)."</li>"; 
					break;
					case 'share' :
						if($sfsi_premium_section1_options['sfsi_plus_share_display']=='yes')    $icons.= "<li id='SFshareIcon'>".sfsi_plus_prepairIcons('share',1)."</li>";
					break;
					case 'youtube' :
						if($sfsi_premium_section1_options['sfsi_plus_youtube_display']=='yes')     $icons.= "<li>".sfsi_plus_prepairIcons('youtube',1)."</li>"; 
					break;
					case 'pinterest' :
						if($sfsi_premium_section1_options['sfsi_plus_pinterest_display']=='yes')     $icons.= "<li>".sfsi_plus_prepairIcons('pinterest',1)."</li>";
					break;
					case 'linkedin' :
						if($sfsi_premium_section1_options['sfsi_plus_linkedin_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('linkedin',1)."</li>"; 
					break;
					case 'instagram' :
						if($sfsi_premium_section1_options['sfsi_plus_instagram_display']=='yes')    $icons.= "<li>".sfsi_plus_prepairIcons('instagram',1)."</li>"; 
					break;
					case 'houzz' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_houzz_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_houzz_display']=='yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('houzz',1)."</li>";
						}
					break;
					case 'snapchat' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_snapchat_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_snapchat_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('snapchat',1)."</li>";
						}
					break;
					case 'whatsapp' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_whatsapp_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_whatsapp_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('whatsapp',1)."</li>";
						}
					break;
					case 'skype' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_skype_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_skype_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('skype',1)."</li>";
						}
					break;
					case 'vimeo' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_vimeo_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_vimeo_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('vimeo',1)."</li>";
						}
					break;
					case 'soundcloud' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_soundcloud_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_soundcloud_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('soundcloud',1)."</li>";
						}
					break;
					case 'yummly' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_yummly_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_yummly_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('yummly',1)."</li>";
						}
					break;
					case 'flickr' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_flickr_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_flickr_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('flickr',1)."</li>";
						}
					break;
					case 'reddit' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_reddit_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_reddit_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('reddit',1)."</li>";
						}
					break;
					case 'tumblr' :
						if(
							isset($sfsi_premium_section1_options['sfsi_plus_tumblr_display']) &&
							$sfsi_premium_section1_options['sfsi_plus_tumblr_display'] == 'yes'
						)
						{
							$icons.= "<li>".sfsi_plus_prepairIcons('tumblr',1)."</li>";
						}
					break;
					case 'custom' :
						$icons.= "<li>". sfsi_plus_prepairIcons($icon_arry['ele'],1)."</li>"; 
					break;
				endswitch;
			}
		}
	}
	$icons.='</ul>';

	return $icons;	        
}

function sfsi_plus_subscribe_form_html($ulmargin){
	$icons.='<ul style="'.$ulmargin.'">';
	$icons.= sfsi_plus_get_subscriberForm();
	$icons.= '</ul>';
	return  $icons;
}
?>