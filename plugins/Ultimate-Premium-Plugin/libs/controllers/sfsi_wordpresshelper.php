<?php
function sfsi_premium_is_ssl(){
	$scheme = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
	return $scheme;
}

function sfsi_get_description($postid)
{
	$post = get_post($postid);
	$desc = trim(get_the_excerpt($postid));

	if(strlen($desc)==0){
        $desc = $post->post_content;
		$desc = str_replace(']]>',']]&gt;', $desc);
        $desc = strip_shortcodes( $desc );
	}

	$desc 	= strip_tags( $desc );
	$desc   = esc_attr($desc);
	$desc   = trim(preg_replace("/\s+/", " ", $desc));
	$desc   = sfsi_sub_string($desc, 400);
	return $desc;
}

function sfsi_filter_text($desc)
{
	if(strlen($desc)>0){
 		$desc 	= str_replace(']]>',']]&gt;', $desc);
        $desc 	= strip_shortcodes( $desc );
		$desc 	= strip_tags( $desc );
		$desc   = esc_attr($desc);
		$desc   = trim(preg_replace("/\s+/", " ", $desc));
	}
	return $desc;
}

function sfsi_sub_string($text, $charlength=200) {
	$charlength++;
	$retext="";
	if ( mb_strlen( $text ) > $charlength ) {
		$subex = mb_substr( $text, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$retext .= mb_substr( $subex, 0, $excut );
		} else {
			$retext .= $subex;
		}
		$retext .= '[...]';
	} else {
		$retext .= $text;
	}
	
	return $retext;
}

/**
 * Get an attachment ID given a URL.
 * 
 * @param string $url
 *
 * @return int Attachment ID on success, 0 on failure
 */
function sfsi_get_attachment_id( $url ) { 
	$attachment_id = false;
	global $wpdb; $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $url ));
	$attachment_id = (isset($attachment[0]) && !empty($attachment[0])) ? $attachment[0]:$attachment_id; 
	return $attachment[0]; 
}

//sanitizing values
function sfsi_plus_string_sanitize($s) {
    $result = preg_replace("/[^a-zA-Z0-9]+/", " ", html_entity_decode($s, ENT_QUOTES));
    return $result;
}

function sfsi_plus_get_bloginfo($url)
{
	$web_url = get_bloginfo($url);
	
	//Block to use feedburner url
	if (preg_match("/(feedburner)/im", $web_url, $match))
	{
		$web_url = site_url()."/feed";
	}
	return $web_url;
}

add_shortcode("usm_premium_shared_current_url","sfsi_plus_current_url");
function sfsi_plus_current_url()
{
	global $post, $wp;

	if (!empty($wp)) {
		return home_url(add_query_arg(array(),$wp->request));
	}
	elseif(!empty($post))
	{
		return get_permalink($post->ID);
	}
	else
	{
		return site_url();
	}
}
?>