<?php 

function sfsi_is_rectangle_icons_shortcode_showing_on_front(){
	
	$sfsi_section8 = unserialize(get_option("sfsi_premium_section8_options"));

	$isDisplayingOnMobile  = (isset($sfsi_section8['sfsi_plus_rectangle_icons_shortcode_show_on_mobile']) && $sfsi_section8['sfsi_plus_rectangle_icons_shortcode_show_on_mobile'] == 'yes');
	$isDisplayingOnDesktop = (isset($sfsi_section8['sfsi_plus_rectangle_icons_shortcode_show_on_desktop']) && $sfsi_section8['sfsi_plus_rectangle_icons_shortcode_show_on_desktop'] == 'yes');
	
	$isDisplayingRectangleIconsOnFront = 	$isDisplayingOnDesktop || $isDisplayingOnMobile;

	return $isDisplayingRectangleIconsOnFront;
}

function sfsi_is_icons_showing_on_front(){

	$isIconsDisplayingOnFront = false;

	$options8 = unserialize(get_option("sfsi_premium_section8_options"));
	$options7 = unserialize(get_option("sfsi_premium_section7_options"));

	// Check if icons are displayed from Question 3
	$arrKeys  = array('sfsi_plus_show_via_widget','sfsi_plus_float_on_page','sfsi_plus_place_item_manually','sfsi_plus_show_item_onposts');

	foreach($arrKeys as $value) {
		$isIconsDisplayingOnFront = (false != isset($options8[$value]) && "yes"== $options8[$value]) ? true : $isIconsDisplayingOnFront;
		if($isIconsDisplayingOnFront) { break;}	
	} 

	// Check if rectangle icons are displayed using shortcode on any location from Question 3
	 if(false != sfsi_is_rectangle_icons_shortcode_showing_on_front()){
		$isIconsDisplayingOnFront = false;
	 }	

	// Check if popup is displayed from Question 3
	 if(false != sfsi_is_rectangle_icons_shortcode_showing_on_front()){
		$isIconsDisplayingOnFront = true;
	 }

	// Check if popup is displayed from Question 7
	if(false != isset($options7['sfsi_plus_Show_popupOn']) && "none" != $options7['sfsi_plus_Show_popupOn']){
		$isIconsDisplayingOnFront = true;
	}

	return $isIconsDisplayingOnFront;	
}

/*  instalation of javascript and css  */
function sfsiplus_plugin_back_enqueue_script()
{
		
	if(isset($_GET['page']))
	{
		if($_GET['page'] == 'sfsi-plus-options')
		{
			wp_enqueue_style('thickbox');
			wp_enqueue_style("SFSIPLUSmainCss", SFSI_PLUS_PLUGURL . 'css/sfsi-style.css' );
			
			wp_enqueue_style("SFSIPLUSJqueryCSS", SFSI_PLUS_PLUGURL . 'css/jquery-ui-1.10.4/jquery-ui-min.css' );
			wp_enqueue_style("wp-color-picker");

			wp_register_style( 'bootstrap.min', SFSI_PLUS_PLUGURL . 'css/bootstrap.min.css' );
			wp_enqueue_style('bootstrap.min');

			/* include CSS for backend */
			wp_enqueue_style("SFSIPLUSmainadminCss", SFSI_PLUS_PLUGURL . 'css/sfsi-admin-style.css' );

		}
	}
		
	if(isset($_GET['page']))
	{
		if($_GET['page'] == 'sfsi-plus-options')
		{
			wp_enqueue_script('jquery');
			wp_enqueue_script("jquery-migrate");
			
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			
			wp_enqueue_script("jquery-ui-accordion");	
			wp_enqueue_script("wp-color-picker");
			wp_enqueue_script("jquery-effects-core");
			wp_enqueue_script("jquery-ui-sortable");
				
			
			wp_register_script('SFSIPLUSJqueryFRM', SFSI_PLUS_PLUGURL . 'js/jquery.form-min.js', '', '', true);
			wp_enqueue_script("SFSIPLUSJqueryFRM");
			
			wp_register_script('SFSIPLUSCustomFormJs', SFSI_PLUS_PLUGURL . 'js/custom-form-min.js', '', '', true);
			wp_enqueue_script("SFSIPLUSCustomFormJs");
			
			wp_register_script('SFSIPLUSCustomJs', SFSI_PLUS_PLUGURL . 'js/custom-admin.js', '', '', true);

			//Bootstrap Scripts
			wp_register_script('bootstrap.min', SFSI_PLUS_PLUGURL.'js/bootstrap.min.js');
			wp_enqueue_script('bootstrap.min');

			
			// Localize the script with new data
			$translation_array = array(
				'Re_ad'                 => __('Read more',SFSI_PLUS_DOMAIN),
				'Sa_ve'                 => __('Save',SFSI_PLUS_DOMAIN),
				'Sav_ing'               => __('Saving',SFSI_PLUS_DOMAIN),
				'Sa_ved'                => __('Saved',SFSI_PLUS_DOMAIN)
			);
			$translation_array1 = array(
				'Coll_apse'             => __('Collapse',SFSI_PLUS_DOMAIN),
				'Save_All_Settings'     => __('Save All Settings',SFSI_PLUS_DOMAIN),
				'Upload_a'    			=> __('Upload a custom icon if you have other accounts/websites you want to link to.',SFSI_PLUS_DOMAIN),
				'It_depends'     		=> __('It depends',SFSI_PLUS_DOMAIN)
			);
			
			wp_localize_script( 'SFSIPLUSCustomJs', 'object_name', $translation_array );
			wp_localize_script( 'SFSIPLUSCustomJs', 'object_name1', $translation_array1 );
			wp_enqueue_script("SFSIPLUSCustomJs");
			
			wp_register_script('SFSIPLUSCustomValidateJs', SFSI_PLUS_PLUGURL . 'js/customValidate-min.js', '', '', true);
			wp_enqueue_script("SFSIPLUSCustomValidateJs");
			/* end cusotm js */
			
			/* initilaize the ajax url in javascript */
			wp_localize_script( 'SFSIPLUSCustomJs', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
			wp_localize_script( 'SFSIPLUSCustomValidateJs', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ),'plugin_url'=> SFSI_PLUS_PLUGURL) );
		}
	}
}
add_action( 'admin_enqueue_scripts', 'sfsiplus_plugin_back_enqueue_script' );

function sfsiplus_plugin_front_enqueue_script()
{
	if(sfsi_is_icons_showing_on_front()){
		
		wp_enqueue_style("SFSIPLUSmainCss", SFSI_PLUS_PLUGURL . 'css/sfsi-style.css' );
		
		$option5=  unserialize(get_option('sfsi_premium_section5_options',false));
		
		if($option5['sfsi_plus_disable_floaticons'] == 'yes')
		{
			wp_enqueue_style("disable_sfsiplus", SFSI_PLUS_PLUGURL . 'css/disable_sfsi.css' );
		}
		
		wp_enqueue_script('jquery');
	 	wp_enqueue_script("jquery-migrate");
		wp_enqueue_script('jquery-ui-core');	
		
		wp_register_script('SFSIPLUSjqueryModernizr', SFSI_PLUS_PLUGURL . 'js/shuffle/modernizr.custom.min.js', '','',true);
		wp_enqueue_script("SFSIPLUSjqueryModernizr");
		
		wp_register_script('SFSIPLUSjqueryShuffle', SFSI_PLUS_PLUGURL . 'js/shuffle/jquery.shuffle.min.js', '','',true);
		wp_enqueue_script("SFSIPLUSjqueryShuffle");
		
		wp_register_script('SFSIPLUSjqueryrandom-shuffle', SFSI_PLUS_PLUGURL . 'js/shuffle/random-shuffle-min.js', '','',true);
		wp_enqueue_script("SFSIPLUSjqueryrandom-shuffle");
		
		wp_register_script('SFSIPLUSCustomJs', SFSI_PLUS_PLUGURL . 'js/custom.js', '', '', true);
		wp_enqueue_script("SFSIPLUSCustomJs");
		/* end cusotm js */

		/* initilaize the ajax url in javascript */
		wp_localize_script( 'SFSIPLUSCustomJs', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ),'plugin_url'=> SFSI_PLUS_PLUGURL) );
	}
}
add_action( 'wp_enqueue_scripts', 'sfsiplus_plugin_front_enqueue_script' );

//Check page excluded or not
function sfsi_plus_icon_exclude()
{
	$option8 = unserialize(get_option('sfsi_premium_section8_options',false));
 
 	if(is_archive()){

		if(isset($option8['sfsi_plus_switch_exclude_taxonomies']) && $option8['sfsi_plus_switch_exclude_taxonomies']=="yes"){

			$arrSfsi_plus_list_exclude_taxonomies = (isset($option8['sfsi_plus_list_exclude_taxonomies'])) ? unserialize($option8['sfsi_plus_list_exclude_taxonomies']) : array();

			if(count(array_filter($arrSfsi_plus_list_exclude_taxonomies))>0){
				
				$termData = get_queried_object();
				
				if(in_array($termData->taxonomy, $arrSfsi_plus_list_exclude_taxonomies)) {
					return true;			
				}
				else
				{
					if($option8['sfsi_plus_exclude_url'] == 'yes')
					{
						return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
					}
				}				
			}
			else
			{
				if($option8['sfsi_plus_exclude_url'] == 'yes')
				{
					return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
				}
			}
		}
		else{
				if($option8['sfsi_plus_exclude_url'] == 'yes')
				{
					return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
				}			
		}
 	} 
	else if (is_single())
	{
		if($option8['sfsi_plus_exclude_post'] == 'yes'){ return true; }

		else if(isset($option8['sfsi_plus_switch_exclude_custom_post_types']) && $option8['sfsi_plus_switch_exclude_custom_post_types']=="yes")
		{

			$arrSfsi_plus_list_exclude_custom_post_types = (isset($option8['sfsi_plus_list_exclude_custom_post_types'])) ? unserialize($option8['sfsi_plus_list_exclude_custom_post_types']) : array();

			if(count(array_filter($arrSfsi_plus_list_exclude_custom_post_types))>0){

				$socialObj 		= new sfsi_plus_SocialHelper();
				$post_id 	    = $socialObj->sfsi_get_the_ID();
				$curr_post_type = get_post_type($post_id);

				if($post_id && in_array($curr_post_type, $arrSfsi_plus_list_exclude_custom_post_types)){ 
					return true;			
				}
				else
				{
					if($option8['sfsi_plus_exclude_url'] == 'yes')
					{
						return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
					}
				}									
			}
			else
			{
				if($option8['sfsi_plus_exclude_url'] == 'yes')
				{
					return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
				}
			}		
		}		
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_singular() && !is_front_page())
	{
		if($option8['sfsi_plus_exclude_page'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_front_page())
	{
		if($option8['sfsi_plus_exclude_home'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_tag())
	{
		if($option8['sfsi_plus_exclude_tag'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_category())
	{
		if($option8['sfsi_plus_exclude_category'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_date())
	{
		if($option8['sfsi_plus_exclude_date_archive'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_author())
	{
		if($option8['sfsi_plus_exclude_author_archive'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else if (is_search())
	{
		if($option8['sfsi_plus_exclude_search'] == 'yes'){ return true; }
		else
		{
			if($option8['sfsi_plus_exclude_url'] == 'yes')
			{
				return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
			}
		}
	}
	else
	{
		return sfsi_plus_icon_exclude_from_url($option8['sfsi_plus_urlKeywords']);
	}
}

//Check exclude from url or not
function sfsi_plus_icon_exclude_from_url($keywords)
{	
	global $wp;
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ));
	$current_url = $current_url."/";

	if(false != isset($keywords) && is_array($keywords)){
	
		$count		 = count($keywords);

			for($i = 0; $i < $count; $i++)
			{
				if (strpos(strtolower($current_url), strtolower($keywords[$i]))) {
					return true;
				}
			}			
	}
}

function whatsapp_js(){ 
	wp_register_script('SFSIPLUSWhatsappJs', SFSI_PLUS_PLUGURL . 'js/whatsapp.js', '', '', true);
	wp_enqueue_script("SFSIPLUSWhatsappJs");
}
add_action('wp_head','whatsapp_js');
?>