<?php
//Add Subscriber form css
// Creating the widget 
class sfsiPlus_subscriber_widget extends WP_Widget {

	function __construct()
	{
		parent::__construct(
			// Base ID of your widget
			'sfsiPlus_subscriber_widget', 
	
			// Widget name will appear in UI
			'Ultimate Premium Subscribe Form', 
	
			// Widget description
			array( 'description' => 'Ultimate Social Plus Subscribe Form') 
		);
	}

	public function widget( $args, $instance )
	{
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		if ( ! empty( $title ) )
		{
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// Call subscriber form
		echo do_shortcode("[USM_plus_form]");
		
		echo $args['after_widget'];
	}
		
	// Widget Backend 
	public function form( $instance )
	{
		if ( isset( $instance[ 'title' ] ))
		{
			$title = $instance[ 'title' ];
		}
		else
		{
			$title = '';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title' ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $newInstance, $oldInstance )
	{
		$instance = array();
		$instance['title'] = ( ! empty( $newInstance['title'] ) ) ? strip_tags( $newInstance['title'] ) : '';
		return $instance;
	}
}
// Class wpb_widget ends here

// Register and load the widget
function sfsiPlus_subscriber_load_widget()
{
	register_widget( 'sfsiPlus_subscriber_widget' );
}
add_action( 'widgets_init', 'sfsiPlus_subscriber_load_widget' );
?>
<?php
add_shortcode("USM_plus_form", "sfsi_plus_get_subscriberForm");
function sfsi_plus_get_subscriberForm()
{
	$option9 			= unserialize(get_option('sfsi_premium_section9_options',false));
	$sfsi_plus_feediid 	= sanitize_text_field(get_option('sfsi_premium_feed_id'));
	$url = "https://www.specificfeeds.com/widgets/subscribeWidget/";
	
	$return = '';
	$url = $url.$sfsi_plus_feediid.'/8/';	
	$return .= '<div class="sfsi_plus_subscribe_Popinner">
					<form method="post" onsubmit="return sfsi_plus_processfurther(this);" target="popupwindow" action="'.$url.'">
						<h5>'.trim($option9['sfsi_plus_form_heading_text']).'</h5>
						<div class="sfsi_plus_subscription_form_field">
							<input type="email" name="data[Widget][email]" value="" placeholder="'.trim($option9['sfsi_plus_form_field_text']).'"/>
						</div>
						<div class="sfsi_plus_subscription_form_field">
							<input type="hidden" name="data[Widget][feed_id]" value="'.$sfsi_plus_feediid.'">
							<input type="hidden" name="data[Widget][feedtype]" value="8">
							<input type="submit" name="subscribe" value="'.$option9['sfsi_plus_form_button_text'].'" />
						</div>
					</form>
				</div>';
	return $return;			
}
?>