<?php
/*
Plugin Name: USM Premium
Plugin URI: https://www.ultimatelysocial.com
Description: The best social media plugin on the market. Allows you to add social media & share icons to your blog (esp. Facebook, Twitter, Email, RSS, Pinterest, Instagram, Google+, LinkedIn, Share-button). It offers a wide range of design options and other features. 
Author: UltimatelySocial
Text Domain: usm-premium-icons
Domain Path: /languages
Author URI: https://www.ultimatelysocial.com
Version: 5.1
License: GPLv2
*/

global $wpdb;

/* define the Root for URL and Document */
define( 'ULTIMATE_STORE_URL', 'https://www.ultimatelysocial.com' );
define( 'ULTIMATE_ITEM_NAME', 'USM Premium Plugin' );
define( 'ULTIMATE_PLUGIN_LICENSE_PAGE', 'ultimate-license' );

define('SFSI_PLUS_DOCROOT',    dirname(__FILE__));
define('SFSI_PLUS_PLUGURL',    plugin_dir_url(__FILE__));
define('SFSI_PLUS_WEBROOT',    str_replace(getcwd(), home_url(), dirname(__FILE__)));
define('SFSI_PLUS_DOMAIN',	   'usm-premium-icons');

/* load all files  */
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_wordpresshelper.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_Init_JqueryCss.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/class.sfsiCumulativeCount.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_socialhelper.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_install_uninstall.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_buttons_controller.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_iconsUpload_contoller.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_floater_icons.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_frontpopUp.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsiocns_OnPosts.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_widget.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_css_settings.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_plus_subscribe_widget.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_newsletterSubscription.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_urlShortner.php');
include(SFSI_PLUS_DOCROOT.'/libs/sfsi_custom_social_sharing_data.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_shortcodes.php');
include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_metatags.php');

/* plugin install and uninstall hooks */ 
register_activation_hook(__FILE__, 'sfsi_premium_activate_plugin' );
register_deactivation_hook(__FILE__, 'sfsi_plus_deactivate_plugin');
register_uninstall_hook(__FILE__, 'sfsi_plus_Unistall_plugin');

/*Plugin version setup*/
if(!get_option('sfsi_premium_pluginVersion') || get_option('sfsi_premium_pluginVersion') < 5.1)
{
	add_action("init", "sfsi_plus_update_plugin");
}

/*Ultimate Premium*/
if( !class_exists( 'Ultimate_Plugin_Updater' ) ) {
	// load our custom updater
	include( dirname( __FILE__ ) . '/ultimate_plugin_updater.php' );
}

/*Ultimate Premium Updater init*/
function ultimate_plugin_updater()
{
	// retrieve our license key from the DB
	$license_key = trim( get_option( 'ultimate_license_key' ) );

	// setup the updater
	$edd_updater = new Ultimate_Plugin_Updater( ULTIMATE_STORE_URL, __FILE__, array(
			'version'   => '5.1',				// current version number
			'license'   => $license_key,        // license key (used get_option above to retrieve from DB)
			'item_name' => ULTIMATE_ITEM_NAME, 	// name of this plugin
			'author'    => 'UltimatelySocial'  	// author of this plugin
		)
	);
}
add_action('admin_init', 'ultimate_plugin_updater', 0 );


include(SFSI_PLUS_DOCROOT.'/libs/controllers/sfsi_licensing.php');


//Get verification code
if(is_admin())
{	
	$code 		= sanitize_text_field(get_option('sfsi_premium_verificatiom_code'));
	$feed_id 	= sanitize_text_field(get_option('sfsi_premium_feed_id'));
	if(empty($code) && !empty($feed_id))
	{
		add_action("init", "sfsi_plus_getverification_code");
	}
}

function sfsi_plus_getverification_code()
{
	$feed_id = sanitize_text_field(get_option('sfsi_premium_feed_id'));
	$curl = curl_init();  
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://www.specificfeeds.com/wordpress/getVerifiedCode_plugin',
        CURLOPT_USERAGENT => 'sf get verification',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'feed_id' => $feed_id
        )
    ));
    
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	$resp = json_decode($resp);
	update_option('sfsi_premium_verificatiom_code', $resp->code);
	curl_close($curl);
}

function sfsi_get_before_posts_icons(){

	$icons_before  = '';

	$socialObj    = new sfsi_plus_SocialHelper();
	$postid       = $socialObj->sfsi_get_the_ID();
		
	if($postid){

		$sfsi_section8 =  unserialize(get_option('sfsi_premium_section8_options',false));
		$sfsi_section5 =  unserialize(get_option('sfsi_premium_section5_options',false));

		$lineheight = $sfsi_section8['sfsi_plus_post_icons_size'];
		$lineheight = sfsi_plus_getlinhght($lineheight);

		$sfsi_plus_display_button_type = $sfsi_section8['sfsi_plus_display_button_type'];
		$sfsi_plus_show_item_onposts   = $sfsi_section8['sfsi_plus_show_item_onposts'];

		$post         = get_post($postid);
		$permalink    = get_permalink($postid);
		$post_title   = $post->post_title;
		$sfsiLikeWith = "45px;";

			if($sfsi_section8['sfsi_plus_icons_DisplayCounts']=="yes")
			{
				$show_count=1;
				$sfsiLikeWith="75px;";
			}   
			else
			{
				$show_count=0;
			} 
			
			//checking for standard icons
			if(!isset($sfsi_section8['sfsi_plus_rectsub']))
			{
				$sfsi_section8['sfsi_plus_rectsub'] = 'no';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectfb']))
			{
				$sfsi_section8['sfsi_plus_rectfb'] = 'yes';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectgp']))
			{
				$sfsi_section8['sfsi_plus_rectgp'] = 'yes';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectshr']))
			{
				$sfsi_section8['sfsi_plus_rectshr'] = 'yes';
			}
			if(!isset($sfsi_section8['sfsi_plus_recttwtr']))
			{
				$sfsi_section8['sfsi_plus_recttwtr'] = 'no';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectpinit']))
			{
				$sfsi_section8['sfsi_plus_rectpinit'] = 'no';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectfbshare']))
			{
				$sfsi_section8['sfsi_plus_rectfbshare'] = 'no';
			}
			
			//checking for standard icons
			$txt=(isset($sfsi_section8['sfsi_plus_textBefor_icons']))? $sfsi_section8['sfsi_plus_textBefor_icons'] : "Please follow and like us:" ;
			
			$float = $sfsi_section8['sfsi_plus_icons_alignment'];
			if($float == "center")
			{
				$style_parent= 'text-align: center;';
				$style = 'float:none; display: inline-block;';
			}
			else
			{
				$style_parent= 'text-align:'.$float;
				$style = 'float:'.$float;
			}

			//icon selection
			$icons_before .= "<div class='sfsibeforpstwpr' style='".$style_parent."'>";
				$icons_before .= "<div class='sfsi_plus_Sicons ".$float."' style='".$style."'>";
					if($sfsi_plus_display_button_type == 'standard_buttons')
					{
							if(
								$sfsi_section8['sfsi_plus_rectsub']		== 'yes' ||
								$sfsi_section8['sfsi_plus_rectfb']		== 'yes' ||
								$sfsi_section8['sfsi_plus_rectgp']		== 'yes' ||
								$sfsi_section8['sfsi_plus_rectshr'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_recttwtr'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectpinit'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectlinkedin']== 'yes' ||
								$sfsi_section8['sfsi_plus_rectreddit'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectfbshare'] == 'yes' 
							)
							{
								$icons_before .= "<div style='display: inline-block;margin-bottom: 0; margin-left: 0; margin-right: 8px; margin-top: 0; vertical-align: middle;width: auto;'><span>".$txt."</span></div>";
							}
							if($sfsi_section8['sfsi_plus_rectsub'] == 'yes')
							{
								if($show_count){$sfsiLikeWithsub = "93px";}else{$sfsiLikeWithsub = "64px";}
								if(!isset($sfsiLikeWithsub)){$sfsiLikeWithsub = $sfsiLikeWith;}
								$icons_before.="<div class='sf_subscrbe' style='display: inline-block;vertical-align: middle;width: auto;'>".sfsi_plus_Subscribelike($permalink,$show_count)."</div>";
							}
							// if($sfsi_section8['sfsi_plus_rectfb'] == 'yes' || $sfsi_section8['sfsi_plus_rectfbshare'] == 'yes')
							// {
							// 	if($show_count){}else{$sfsiLikeWithfb = "48px";}
							// 	if(!isset($sfsiLikeWithfb)){$sfsiLikeWithfb = $sfsiLikeWith;}
							// 	if($sfsi_section5['sfsi_plus_Facebook_linking'] == "facebookcustomurl")
						 //        {
						 //        	$userDefineLink = ($sfsi_section5['sfsi_plus_facebook_linkingcustom_url']);
						 //        	$icons_before .= "<div class='sf_fb' style='display: inline-block; vertical-align: middle;width: auto;'>".sfsi_plus_FBlike($userDefineLink,$show_count)."</div>";
						 //        }
						 //        else
						 //        {  
						 //        	$icons_before .= "<div class='sf_fb' style='display: inline-block; vertical-align: middle;width: auto;'>".sfsi_plus_FBlike($permalink,$show_count)."</div>";

						 //        }								
							// }

							if($sfsi_section8['sfsi_plus_rectfb'] == 'yes')
							{
								if($show_count){}else{$sfsiLikeWithfb = "48px";}
								if(!isset($sfsiLikeWithfb)){$sfsiLikeWithfb = $sfsiLikeWith;}
						        
						        if($sfsi_section5['sfsi_plus_Facebook_linking'] == "facebookcustomurl")
						        {
						        	$userDefineLink = ($sfsi_section5['sfsi_plus_facebook_linkingcustom_url']);
						        	$icons_before .="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_FBlike($userDefineLink,$show_count)."</div>";
						        }
						        else
						        {
									$icons_before .="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_FBlike($permalink,$show_count)."</div>";
								}
							}

							if($sfsi_section8['sfsi_plus_rectfbshare'] == 'yes')
							{
								if($show_count){}else{$sfsiLikeWithfb = "48px";}
								$permalink = $socialObj->sfsi_get_custom_share_link('facebook');        	
								$icons_before .="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsiFB_Share($permalink,$show_count)."</div>";
							}

							if($sfsi_section8['sfsi_plus_recttwtr'] == 'yes')
							{
								if($show_count){$sfsiLikeWithtwtr = "77px";}else{$sfsiLikeWithtwtr = "56px";}
								if(!isset($sfsiLikeWithtwtr)){$sfsiLikeWithtwtr = $sfsiLikeWith;}

								$permalink = $socialObj->sfsi_get_custom_share_link('twitter');
								$icons_before.="<div class='sf_twiter' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_twitterlike($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectpinit'] == 'yes')
							{
								if($show_count){$sfsiLikeWithpinit = "100px";}else{$sfsiLikeWithpinit = "auto";}
								$icons_before.="<div class='sf_pinit' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithpinit."'>".sfsi_plus_pinitpinterest($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectlinkedin'] == 'yes')
							{
								if($show_count){$sfsiLikeWithlinkedin = "100px";}else{$sfsiLikeWithlinkedin = "auto";}
								$icons_before.="<div class='sf_linkedin' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithlinkedin."'>".sfsi_LinkedInShare($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectreddit'] == 'yes')
							{
								if($show_count){$sfsiLikeWithreddit = "auto";}else{$sfsiLikeWithreddit = "auto";}
								$icons_before.="<div class='sf_reddit' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithreddit."'>".sfsi_redditShareButton($permalink)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectgp'] == 'yes')
							{
								if($show_count){$sfsiLikeWithpingogl = "63px";}else{$sfsiLikeWithpingogl = "auto";}
								$icons_before .= "<div class='sf_google'  style='display: inline-block;vertical-align: middle;width: ".$sfsiLikeWithpingogl.";'>".sfsi_plus_googlePlus($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectshr'] == 'yes')
							{
								$icons_before .= "<div class='sf_addthis'  style='display: inline-block;vertical-align: middle;width: auto;margin-top: 6px;'>".sfsi_plus_Addthis_blogpost($show_count, $permalink, $post_title)."</div>";
							}
					}
					else
					{
						$icons_before .= "<div style='float:left;margin:0 0px; line-height:".$lineheight."px'><span>".$txt."</span></div>";
						$icons_before .= sfsi_plus_check_posts_visiblity(0 , "yes");
					}
				$icons_before .= "</div>";
			$icons_before .= "</div>";
	}
	return $icons_before;	
}

function sfsi_get_after_posts_icons(){

	$icons_after   = '';
		
	$socialObj    = new sfsi_plus_SocialHelper();
	$postid       = $socialObj->sfsi_get_the_ID();

	if($postid){

			$sfsi_section8 =  unserialize(get_option('sfsi_premium_section8_options',false));
			$sfsi_section5 =  unserialize(get_option('sfsi_premium_section5_options',false));

			$lineheight = $sfsi_section8['sfsi_plus_post_icons_size'];
			$lineheight = sfsi_plus_getlinhght($lineheight);

			$sfsi_plus_display_button_type = $sfsi_section8['sfsi_plus_display_button_type'];
			$sfsi_plus_show_item_onposts   = $sfsi_section8['sfsi_plus_show_item_onposts'];

			$post         = get_post($postid);
			$permalink    = get_permalink($postid);
			$post_title   = $post->post_title;
			$sfsiLikeWith = "45px;";

			if($sfsi_section8['sfsi_plus_icons_DisplayCounts']=="yes")
			{
				$show_count=1;
				$sfsiLikeWith="75px;";
			}   
			else
			{
				$show_count=0;
			} 
			
			//checking for standard icons
			if(!isset($sfsi_section8['sfsi_plus_rectsub']))
			{
				$sfsi_section8['sfsi_plus_rectsub'] = 'no';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectfb']))
			{
				$sfsi_section8['sfsi_plus_rectfb'] = 'yes';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectgp']))
			{
				$sfsi_section8['sfsi_plus_rectgp'] = 'yes';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectshr']))
			{
				$sfsi_section8['sfsi_plus_rectshr'] = 'yes';
			}
			if(!isset($sfsi_section8['sfsi_plus_recttwtr']))
			{
				$sfsi_section8['sfsi_plus_recttwtr'] = 'no';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectpinit']))
			{
				$sfsi_section8['sfsi_plus_rectpinit'] = 'no';
			}
			if(!isset($sfsi_section8['sfsi_plus_rectfbshare']))
			{
				$sfsi_section8['sfsi_plus_rectfbshare'] = 'no';
			}
			
			//checking for standard icons
			$txt=(isset($sfsi_section8['sfsi_plus_textBefor_icons']))? $sfsi_section8['sfsi_plus_textBefor_icons'] : "Please follow and like us:" ;
			
			$float = $sfsi_section8['sfsi_plus_icons_alignment'];
			if($float == "center")
			{
				$style_parent= 'text-align: center;';
				$style = 'float:none; display: inline-block;';
			}
			else
			{
				$style_parent= 'text-align:'.$float;
				$style = 'float:'.$float;
			}
			
			//icon selection
			$icons_after .= "<div class='sfsiaftrpstwpr' style='".$style_parent."'>";
					$icons_after .= "<div class='sfsi_plus_Sicons ".$float."' style='".$style."'>";
						
						if($sfsi_plus_display_button_type == 'standard_buttons')
						{
							if(
								$sfsi_section8['sfsi_plus_rectsub'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectfb'] 		== 'yes' ||
								$sfsi_section8['sfsi_plus_rectgp'] 		== 'yes' ||
								$sfsi_section8['sfsi_plus_rectshr'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_recttwtr'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectpinit'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectlinkedin']== 'yes' ||
								$sfsi_section8['sfsi_plus_rectreddit'] 	== 'yes' ||
								$sfsi_section8['sfsi_plus_rectfbshare'] == 'yes' 
							)
							{
								$icons_after .= "<div style='display: inline-block;margin-bottom: 0; margin-left: 0; margin-right: 8px; margin-top: 0; vertical-align: middle;width: auto;'><span>".$txt."</span></div>";
							}
							if($sfsi_section8['sfsi_plus_rectsub'] == 'yes')
							{
								if($show_count){$sfsiLikeWithsub = "93px";}else{$sfsiLikeWithsub = "64px";}
								if(!isset($sfsiLikeWithsub)){$sfsiLikeWithsub = $sfsiLikeWith;}
								$icons_after.="<div class='sf_subscrbe' style='display: inline-block;vertical-align: middle; width: auto;'>".sfsi_plus_Subscribelike($permalink,$show_count)."</div>";
							}
							// if($sfsi_section8['sfsi_plus_rectfb'] == 'yes' || $sfsi_section8['sfsi_plus_rectfbshare'] == 'yes')
							// {
							// 	if($show_count){}else{$sfsiLikeWithfb = "48px";}
							// 	if(!isset($sfsiLikeWithfb)){$sfsiLikeWithfb = $sfsiLikeWith;}
							// 	if($sfsi_section5['sfsi_plus_Facebook_linking'] == "facebookcustomurl")
						 //        {
						 //        	$userDefineLink = ($sfsi_section5['sfsi_plus_facebook_linkingcustom_url']);
						 //        	$icons_after .= "<div class='sf_fb' style='display: inline-block; vertical-align: middle;width: auto;'>".sfsi_plus_FBlike($userDefineLink,$show_count)."</div>";
						 //        }
						 //        else
						 //        {
						 //        	$icons_after .= "<div class='sf_fb' style='display: inline-block; vertical-align: middle;width: auto;'>".sfsi_plus_FBlike($permalink,$show_count)."</div>";
						 //        }								
							// }
							
							if($sfsi_section8['sfsi_plus_rectfb'] == 'yes')
							{
								if($show_count){}else{$sfsiLikeWithfb = "48px";}
								if(!isset($sfsiLikeWithfb)){$sfsiLikeWithfb = $sfsiLikeWith;}
						        
						        if($sfsi_section5['sfsi_plus_Facebook_linking'] == "facebookcustomurl")
						        {
						        	$userDefineLink = ($sfsi_section5['sfsi_plus_facebook_linkingcustom_url']);
						        	$icons_after .="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_FBlike($userDefineLink,$show_count)."</div>";
						        }
						        else
						        {
									$icons_after .="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsi_plus_FBlike($permalink,$show_count)."</div>";
								}
							}
							if($sfsi_section8['sfsi_plus_rectfbshare'] == 'yes')
							{
								if($show_count){}else{$sfsiLikeWithfb = "48px";}
								$permalink = $socialObj->sfsi_get_custom_share_link('facebook');        	
								$icons_after .="<div class='sf_fb' style='display: inline-block;vertical-align: middle;width: auto;'>".$socialObj->sfsiFB_Share($permalink,$show_count)."</div>";
							}							
							if($sfsi_section8['sfsi_plus_recttwtr'] == 'yes')
							{
								if($show_count){$sfsiLikeWithtwtr = "77px";}else{$sfsiLikeWithtwtr = "56px";}
								if(!isset($sfsiLikeWithtwtr)){$sfsiLikeWithtwtr = $sfsiLikeWith;}

								$permalink = $socialObj->sfsi_get_custom_share_link('twitter');
								$icons_after.="<div class='sf_twiter' style='display: inline-block;vertical-align: middle;width: auto;'>".sfsi_plus_twitterlike($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectpinit'] == 'yes')
							{
								if($show_count){$sfsiLikeWithpinit = "100px";}else{$sfsiLikeWithpinit = "auto";}
							 	$icons_after.="<div class='sf_pinit' style='display: inline-block;text-align:left;vertical-align: middle;width: ".$sfsiLikeWithpinit."'>".sfsi_plus_pinitpinterest($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectlinkedin'] == 'yes')
							{
								if($show_count){$sfsiLikeWithlinkedin = "100px";}else{$sfsiLikeWithlinkedin = "auto";}
								$icons_after.="<div class='sf_linkedin' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithlinkedin."'>".sfsi_LinkedInShare($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectreddit'] == 'yes')
							{
								if($show_count){$sfsiLikeWithreddit = "auto";}else{$sfsiLikeWithreddit = "auto";}
								$icons_after.="<div class='sf_reddit' style='display: inline-block;vertical-align: middle;text-align:left;width: ".$sfsiLikeWithreddit."'>".sfsi_redditShareButton($permalink)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectgp'] == 'yes')
							{
								if($show_count){$sfsiLikeWithpingogl = "63px";}else{$sfsiLikeWithpingogl = "auto";}
								$icons_after .= "<div class='sf_google' style='display: inline-block;vertical-align: middle;width: ".$sfsiLikeWithpingogl.";'>".sfsi_plus_googlePlus($permalink,$show_count)."</div>";
							}
							if($sfsi_section8['sfsi_plus_rectshr'] == 'yes')
							{
								$icons_after .= "<div class='sf_addthis'  style='display: inline-block;vertical-align: middle;width: auto;margin-top: 6px;'>".sfsi_plus_Addthis_blogpost($show_count, $permalink, $post_title)."</div>";
							}
						}
						else
						{
							$icons_after .= "<div style='float:left;margin:0 0px; line-height:".$lineheight."px'><span>".$txt."</span></div>";
							$icons_after .= sfsi_plus_check_posts_visiblity(0 , "yes");
						}
					$icons_after .= "</div>";
			$icons_after .= "</div>";
	}
	return $icons_after;		
}


//functionality for before and after single posts
add_filter( 'the_content', 'sfsi_plus_beforaftereposts' );
function sfsi_plus_beforaftereposts( $content )
{
	$org_content = $content;
	$icons_before = '';
	$icons_after = '';

	global $post;
	$current_post_type = $post->post_type;

	$option8 =  unserialize(get_option('sfsi_premium_section8_options',false));

	$select  =  (isset($option8['sfsi_plus_choose_post_types'])) ? unserialize($option8['sfsi_plus_choose_post_types']) :array(); 
	$select  =  (is_array($select))? $select:array($select); 

	if(!in_array("post", $select)){
		array_push($select,"post");
	}
	
	if(!in_array("page", $select)){
		array_push($select,"page");
	}	
	
	if(count($select)>0 && !empty($select)){
		$cond = is_single() && in_array($current_post_type,$select);
	}

	if($cond)
	{
		$option8=  unserialize(get_option('sfsi_premium_section8_options',false));
		$lineheight = $option8['sfsi_plus_post_icons_size'];
		$lineheight = sfsi_plus_getlinhght($lineheight);
		$sfsi_plus_display_button_type = $option8['sfsi_plus_display_button_type'];
		$txt=(isset($option8['sfsi_plus_textBefor_icons']))? $option8['sfsi_plus_textBefor_icons'] : "Please follow and like us:" ;
		$float = $option8['sfsi_plus_icons_alignment'];
		if($float == "center")
		{
			$style_parent= 'text-align: center;';
			$style = 'float:none; display: inline-block;';
		}
		else
		{
			$style_parent= '';
			$style = 'float:'.$float;
		}
		if($option8['sfsi_plus_display_before_posts'] == "yes" && $option8['sfsi_plus_show_item_onposts'] == "yes")
		{
			$icons_before .= '<div class="sfsibeforpstwpr" style="'.$style_parent.'">';
				if($sfsi_plus_display_button_type == 'standard_buttons')
				{
					$icons_before .= sfsi_plus_social_buttons_below($content = null);
				}
				else
				{
					$icons_before .= "<div class='sfsi_plus_Sicons' style='".$style."'>";
						$icons_before .= "<div style='float:left;margin:0 0px; line-height:".$lineheight."px'><span>".$txt."</span></div>";
						$icons_before .= sfsi_plus_check_posts_visiblity(0 , "yes");
					$icons_before .= "</div>";
				}
			$icons_before .= '</div>';
		}
		if($option8['sfsi_plus_display_after_posts'] == "yes" && $option8['sfsi_plus_show_item_onposts'] == "yes")
		{
			$icons_after .= '<div class="sfsiaftrpstwpr"  style="'.$style_parent.'">';
			if($sfsi_plus_display_button_type == 'standard_buttons')
			{
				$icons_after .= sfsi_plus_social_buttons_below($content = null);
			}
			else
			{
				$icons_after .= "<div class='sfsi_plus_Sicons' style='".$style."'>";
					$icons_after .= "<div style='float:left;margin:0 0px; line-height:".$lineheight."px'><span>".$txt."</span></div>";
					$icons_after .= sfsi_plus_check_posts_visiblity(0 , "yes");
				$icons_after .= "</div>";
			}
			$icons_after .= '</div>';
		}

		if (wp_is_mobile())
		{
			if(isset($option8['sfsi_plus_beforeafterposts_show_on_mobile']) && $option8['sfsi_plus_beforeafterposts_show_on_mobile'] == 'yes'){
				$content = $icons_before.$org_content.$icons_after;
			}
		}
		else{
			if(isset($option8['sfsi_plus_beforeafterposts_show_on_desktop']) && $option8['sfsi_plus_beforeafterposts_show_on_desktop'] == 'yes'){
				$content = $icons_before.$org_content.$icons_after;	
			}						
		}		
	}		
	return $content;
}

//showing before and after blog posts
add_filter( 'the_excerpt', 'sfsi_plus_beforeafterblogposts');
add_filter( 'the_content', 'sfsi_plus_beforeafterblogposts');

function sfsi_plus_beforeafterblogposts( $content )
{
	$org_content   = $content;
	
	$icons_before  = '';
	$icons_after   = '';

	$sfsi_section8 =  unserialize(get_option('sfsi_premium_section8_options',false));	
	
	if(is_front_page()){

		if(isset($sfsi_section8['sfsi_plus_show_item_onposts']) && $sfsi_section8['sfsi_plus_show_item_onposts'] == "yes"){
			
			if($sfsi_section8['sfsi_plus_display_before_blogposts'] == "yes"){
				$icons_before  = sfsi_get_before_posts_icons();
			}

			if($sfsi_section8['sfsi_plus_display_after_blogposts'] == "yes"){
				$icons_after  = sfsi_get_after_posts_icons();
			}										
		}
	}

	// Check if it is Category page for selected taxonomies
	if(is_archive()){

		$arrSfsi_plus_taxonomies_for_icons = (isset($sfsi_section8['sfsi_plus_taxonomies_for_icons'])) ? unserialize($sfsi_section8['sfsi_plus_taxonomies_for_icons']) : array();

		if(count(array_filter($arrSfsi_plus_taxonomies_for_icons))>0){
			
			$termData = get_queried_object();
			
			if(in_array($termData->taxonomy, $arrSfsi_plus_taxonomies_for_icons)) {
				
				if($sfsi_section8['sfsi_plus_display_before_blogposts'] == "yes"){
					$icons_before  = sfsi_get_before_posts_icons();
				}
				if($sfsi_section8['sfsi_plus_display_after_blogposts'] == "yes"){
					$icons_after   = sfsi_get_after_posts_icons();
				}												
			}
		}
	}

	if (wp_is_mobile())
	{
		if(isset($sfsi_section8['sfsi_plus_beforeafterposts_show_on_mobile']) && $sfsi_section8['sfsi_plus_beforeafterposts_show_on_mobile'] == 'yes'){
			$content = $icons_before.$org_content.$icons_after;
		}
	}
	else{
		if(isset($sfsi_section8['sfsi_plus_beforeafterposts_show_on_desktop']) && $sfsi_section8['sfsi_plus_beforeafterposts_show_on_desktop'] == 'yes'){
			$content = $icons_before.$org_content.$icons_after;	
		}						
	}

	return $content;				
}

//showing icons after blog pagespost
add_filter( 'the_excerpt', 'sfsi_plus_afterepages' );
add_filter( 'the_content', 'sfsi_plus_afterepages' );
function sfsi_plus_afterepages( $content )
{
	$org_content = $content;
	$icons_before = '';
	$icons_after = '';
	if(is_page())
	{    
		$option8=  unserialize(get_option('sfsi_premium_section8_options',false));
		$lineheight = $option8['sfsi_plus_post_icons_size'];
		$lineheight = sfsi_plus_getlinhght($lineheight);
		$sfsi_plus_display_button_type = $option8['sfsi_plus_display_button_type'];
		$txt=(isset($option8['sfsi_plus_textBefor_icons']))? $option8['sfsi_plus_textBefor_icons'] : "Please follow and like us:" ;
		$float = $option8['sfsi_plus_icons_alignment'];

		if($float == "center")
		{
			$style_parent= 'text-align: center;';
			$style = 'float:none; display: inline-block;';
		}
		else
		{
			$style_parent= '';
			$style = 'float:'.$float;
		}
		
		if($option8['sfsi_plus_display_after_pageposts'] == "yes" && $option8['sfsi_plus_show_item_onposts'] == "yes")
		{
			/*$icons_after .= '</br>';*/
			$icons_after .= '<div class="sfsiaftrpstwpr"  style="'.$style_parent.'">';
			if($sfsi_plus_display_button_type == 'standard_buttons')
			{
				$icons_after .= sfsi_plus_social_buttons_below($content = null);
			}
			else
			{
				$icons_after .= "<div class='sfsi_plus_Sicons' style='".$style."'>";
					$icons_after .= "<div style='float:left;margin:0 0px; line-height:".$lineheight."px'><span>".$txt."</span></div>";
					$icons_after .= sfsi_plus_check_posts_visiblity(0 , "yes");
				$icons_after .= "</div>";
			}
			$icons_after .= '</div>';
		}
	}

	if (wp_is_mobile())
	{
		if(isset($option8['sfsi_plus_beforeafterposts_show_on_mobile']) && $option8['sfsi_plus_beforeafterposts_show_on_mobile'] == 'yes'){
			$content = $org_content.$icons_after;
		}
	}
	else{
		if(isset($option8['sfsi_plus_beforeafterposts_show_on_desktop']) && $option8['sfsi_plus_beforeafterposts_show_on_desktop'] == 'yes'){
			$content = $org_content.$icons_after;
		}						
	}	
	return $content;
}

//getting line height for the icons
function sfsi_plus_getlinhght($lineheight)
{
	if( $lineheight < 16)
	{
		$lineheight = $lineheight*2;
		return $lineheight;
	}
	elseif( $lineheight >= 16 && $lineheight < 20 )
	{
		$lineheight = $lineheight+10;
		return $lineheight;
	}
	elseif( $lineheight >= 20 && $lineheight < 28 )
	{
		$lineheight = $lineheight+3;
		return $lineheight;
	}
	elseif( $lineheight >= 28 && $lineheight < 40 )
	{
		$lineheight = $lineheight+4;
		return $lineheight;
	}
	elseif( $lineheight >= 40 && $lineheight < 50 )
	{
		$lineheight = $lineheight+5;
		return $lineheight;
	}
	$lineheight = $lineheight+6;
	return $lineheight;
}


add_action('admin_notices', 'sfsi_plus_admin_versionerror', 10);
function sfsi_plus_admin_versionerror()
{   
	if(isset($_GET['page']) && $_GET['page'] == "sfsi-plus-options")
	{
		$style = "overflow: hidden; margin:12px 3px 0px;";
	}
	else
	{
		$style = "overflow: hidden;"; 
	}
	$phpVersion = phpVersion();
	if($phpVersion <= '5.4')
	{
		if(get_option("sfsi_premium_serverphpVersionnotification") == "yes")
		{

		?>
         	<style type="text/css">
			.sfsi_plus_show_phperror_notification {
			   	color: #fff;
			   	text-decoration: underline;
			}
			form.sfsi_plus_phperrorNoticeDismiss {
			    display: inline-block;
			    margin: 5px 0 0;
			    vertical-align: middle;
			}
			.sfsi_plus_phperrorNoticeDismiss input[type='submit']
			{
				background-color: transparent;
			    border: medium none;
			    color: #fff;
			    margin: 0;
			    padding: 0;
			    cursor: pointer;
			}
			.sfsi_plus_show_phperror_notification p{line-height: 22px;}
			p.sfsi_plus_show_notifictaionpragraph{padding: 0 !important;}
		</style>
	     <div class="updated sfsi_plus_show_phperror_notification" style="<?php echo $style; ?>background-color: #D22B2F; color: #fff; font-size: 18px;float:left;border-left-color: #D22B2F;">
			<div class="alignleft" style="margin: 9px 0;">
				<p class="sfsi_plus_show_notifictaionpragraph">
					<?php _e( 'We noticed you are running your site on a PHP version older than 5.4. Please upgrade to a more recent version. This is not only important for running the Ultimate Social Media Plugin, but also for security reasons in general.', SFSI_PLUS_DOMAIN); ?>
					<br>
					<?php _e('If you do not know how to do the upgrade, please ask your server team or hosting company to do it for you.', SFSI_PLUS_DOMAIN); ?>
                </p>
		
			</div>
			<div class="alignright">
				<form method="post" class="sfsi_plus_phperrorNoticeDismiss">
					<input type="hidden" name="sfsi-plus_dismiss-phperrorNotice" value="true">
					<input type="submit" name="dismiss" value="Dismiss" />
				</form>
			</div>
		</div>      
            
		<?php
		}
	}
}	

add_action('admin_init', 'sfsi_plus_dismiss_admin_notice');
function sfsi_plus_dismiss_admin_notice()
{
	if ( isset($_REQUEST['sfsi-plus_dismiss-phperrorNotice']) && $_REQUEST['sfsi-plus_dismiss-phperrorNotice'] == 'true' )
	{
		update_option( 'sfsi_premium_serverphpVersionnotification', "no" );
	}
}

add_action('plugins_loaded', 'sfsi_plus_load_domain');
function sfsi_plus_load_domain() 
{
	$plugin_dir = basename(dirname(__FILE__)).'/languages';
	load_plugin_textdomain( 'ultimate-social-media-plus', false, $plugin_dir );
}


/* plugin action link*/
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'sfsi_plus_action_links', -10 );
function sfsi_plus_action_links ( $mylinks )
{
	$mylinks[] = '<a href="'.admin_url("/admin.php?page=sfsi-plus-options").'">Settings</a>';
	return $mylinks;
}

/* redirect setting page hook */

add_action('admin_init', 'sfsi_plus_plugin_redirect');
function sfsi_plus_plugin_redirect()
{
    if (get_option('sfsi_premium_plugin_do_activation_redirect', false))
    {
        delete_option('sfsi_premium_plugin_do_activation_redirect');
        wp_redirect(admin_url('admin.php?page=sfsi-plus-options'));
    }
}

function sfsi_plus_curl_error_notification()
{   
	if(get_option("sfsi_premium_curlErrorNotices") == "yes")
    {   ?>
        <script type="text/javascript">
			jQuery(document).ready(function(e) {
	            jQuery(".sfsi_plus_curlerror_cross").click(function(){
					SFSI.ajax({
						url:ajax_object.ajax_url,
						type:"post",
						data: {action: "sfsiplus_curlerrornotification"},
						success:function(msg){
							if(msg == 'success')
							{
								jQuery(".sfsiplus_curlerror").hide("fast");
							}
						}
					});
				});
	        });
		</script>
        <div class="sfsiplus_curlerror">
        	<?php _e('We noticed that your site returns a cURL error («Error:', SFSI_PLUS_DOMAIN ); ?>
            <?php echo ucfirst(get_option("sfsi_premium_curlErrorMessage")); ?>
            <?php _e('»). This means that it cannot send a notification to SpecificFeeds.com when a new post is published. Therefore this email-feature doesn’t work. However there are several solutions for this, please visit our FAQ to see the solutions («Perceived bugs» => «cURL error messages»): ', SFSI_PLUS_DOMAIN ); ?>

            <a href="https://www.ultimatelysocial.com/faq/" target="_new">
				<?php _e('www.ultimatelysocial.com/faq', SFSI_PLUS_DOMAIN ); ?>
			</a>
           <div class="sfsi_plus_curlerror_cross"><?php _e('Dismiss', SFSI_PLUS_DOMAIN ); ?></div>
        </div>
        <?php 
    } 
}
?>
