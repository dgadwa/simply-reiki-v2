<?php
     /* unserialize all saved option for Eight options */
    $option8=  unserialize(get_option('sfsi_premium_section8_options',false));
	if(!isset($option8['sfsi_plus_rectsub']))
	{
		$option8['sfsi_plus_rectsub'] = 'no';
	}
	if(!isset($option8['sfsi_plus_rectfb']))
	{
		$option8['sfsi_plus_rectfb'] = 'yes';
	}
	if(!isset($option8['sfsi_plus_rectgp']))
	{
		$option8['sfsi_plus_rectgp'] = 'yes';
	}
	if(!isset($option8['sfsi_plus_rectshr']))
	{
		$option8['sfsi_plus_rectshr'] = 'yes';
	}
	if(!isset($option8['sfsi_plus_recttwtr']))
	{
		$option8['sfsi_plus_recttwtr'] = 'no';
	}
	if(!isset($option8['sfsi_plus_rectpinit']))
	{
		$option8['sfsi_plus_rectpinit'] = 'no';
	}
	if(!isset($option8['sfsi_plus_rectfbshare']))
	{
		$option8['sfsi_plus_rectfbshare'] = 'no';
	}
	if(!isset($option8['sfsi_plus_rectlinkedin']))
	{
		$option8['sfsi_plus_rectlinkedin'] = 'no';
	}
	if(!isset($option8['sfsi_plus_rectreddit']))
	{
		$option8['sfsi_plus_rectreddit'] = 'no';
	}
	
	/**
	 * Sanitize, escape and validate values
	 */
	$option8['sfsi_plus_show_via_widget'] 			= 	(isset($option8['sfsi_plus_show_via_widget']))
															? sanitize_text_field($option8['sfsi_plus_show_via_widget'])
															: '';
	$option8['sfsi_plus_float_on_page'] 			= 	(isset($option8['sfsi_plus_float_on_page']))
															? sanitize_text_field($option8['sfsi_plus_float_on_page'])
															: '';
	$option8['sfsi_plus_make_icon'] 				= 	(isset($option8['sfsi_plus_make_icon']))
															? sanitize_text_field($option8['sfsi_plus_make_icon'])
															: '';
	$option8['sfsi_plus_float_page_position'] 		= 	(isset($option8['sfsi_plus_float_page_position']))
															? sanitize_text_field($option8['sfsi_plus_float_page_position'])
															: '';
	$option8['sfsi_plus_icons_floatMargin_top'] 	= 	(isset($option8['sfsi_plus_icons_floatMargin_top']))
															? intval($option8['sfsi_plus_icons_floatMargin_top'])
															: '';
	$option8['sfsi_plus_icons_floatMargin_bottom'] 	= 	(isset($option8['sfsi_plus_icons_floatMargin_bottom']))
															? intval($option8['sfsi_plus_icons_floatMargin_bottom'])
															: '';
	$option8['sfsi_plus_icons_floatMargin_left'] 	= 	(isset($option8['sfsi_plus_icons_floatMargin_left']))
															? intval($option8['sfsi_plus_icons_floatMargin_left'])
															: '';
	$option8['sfsi_plus_icons_floatMargin_right'] 	= 	(isset($option8['sfsi_plus_icons_floatMargin_right']))
															? intval($option8['sfsi_plus_icons_floatMargin_right'])
															: '';
															
	$option8['sfsi_plus_mobile_float'] 					= 	(isset($option8['sfsi_plus_mobile_float']))
																? sanitize_text_field($option8['sfsi_plus_mobile_float'])
																: 'no';
	$option8['sfsi_plus_make_mobileicon'] 				= 	(isset($option8['sfsi_plus_make_mobileicon']))
																? sanitize_text_field($option8['sfsi_plus_make_mobileicon'])
																: '';														
	$option8['sfsi_plus_float_page_mobileposition'] 	= 	(isset($option8['sfsi_plus_float_page_mobileposition']))
																? sanitize_text_field($option8['sfsi_plus_float_page_mobileposition'])
																: '';
	$option8['sfsi_plus_icons_floatMargin_mobiletop']	= 	(isset($option8['sfsi_plus_icons_floatMargin_mobiletop']))
																? intval($option8['sfsi_plus_icons_floatMargin_mobiletop'])
																: '';
	$option8['sfsi_plus_icons_floatMargin_mobilebottom']= 	(isset($option8['sfsi_plus_icons_floatMargin_mobilebottom']))
																? intval($option8['sfsi_plus_icons_floatMargin_mobilebottom'])
																: '';
	$option8['sfsi_plus_icons_floatMargin_mobileleft'] 	= 	(isset($option8['sfsi_plus_icons_floatMargin_mobileleft']))
																? intval($option8['sfsi_plus_icons_floatMargin_mobileleft'])
																: '';
	$option8['sfsi_plus_icons_floatMargin_mobileright'] = 	(isset($option8['sfsi_plus_icons_floatMargin_mobileright']))
																? intval($option8['sfsi_plus_icons_floatMargin_mobileright'])
																: '';
																																											
	$option8['sfsi_plus_place_item_manually'] 		= 	(isset($option8['sfsi_plus_place_item_manually']))
															? sanitize_text_field($option8['sfsi_plus_place_item_manually'])
															: '';
	$option8['sfsi_plus_place_rectangle_icons_item_manually'] 	= 	(isset($option8['sfsi_plus_place_rectangle_icons_item_manually']))
															? sanitize_text_field($option8['sfsi_plus_place_rectangle_icons_item_manually'])
															: '';
	$option8['sfsi_plus_display_button_type'] 		= 	(isset($option8['sfsi_plus_display_button_type']))
															? sanitize_text_field($option8['sfsi_plus_display_button_type'])
															: '';
	$option8['sfsi_plus_post_icons_size'] 			= 	(isset($option8['sfsi_plus_post_icons_size']))
															? intval($option8['sfsi_plus_post_icons_size'])
															: '';
	$option8['sfsi_plus_post_icons_spacing'] 		= 	(isset($option8['sfsi_plus_post_icons_spacing']))
															? intval($option8['sfsi_plus_post_icons_spacing'])
															: '';
	$option8['sfsi_plus_post_icons_vertical_spacing']= 	(isset($option8['sfsi_plus_post_icons_vertical_spacing']))
															? intval($option8['sfsi_plus_post_icons_vertical_spacing'])
															: '';
															
	$option8['sfsi_plus_show_item_onposts'] 		= 	(isset($option8['sfsi_plus_show_item_onposts']))
															? sanitize_text_field($option8['sfsi_plus_show_item_onposts'])
															: '';
	$option8['sfsi_plus_icons_alignment'] 			= 	(isset($option8['sfsi_plus_icons_alignment']))
															? sanitize_text_field($option8['sfsi_plus_icons_alignment'])
															: '';
	$option8['sfsi_plus_textBefor_icons'] 			= 	(isset($option8['sfsi_plus_textBefor_icons']))
															? sanitize_text_field($option8['sfsi_plus_textBefor_icons'])
															: '';
	$option8['sfsi_plus_textBefor_icons_font_type']	= 	(isset($option8['sfsi_plus_textBefor_icons_font_type']))
															? sanitize_text_field($option8['sfsi_plus_textBefor_icons_font_type'])
															: 'normal';
	$option8['sfsi_plus_textBefor_icons_font_size']	= 	(isset($option8['sfsi_plus_textBefor_icons_font_size']))
															? intval($option8['sfsi_plus_textBefor_icons_font_size'])
															: '20';
	$option8['sfsi_plus_textBefor_icons_font']		=  	(isset($option8['sfsi_plus_textBefor_icons_font']))
															? sanitize_text_field($option8['sfsi_plus_textBefor_icons_font'])
															: 'inherit';
	$option8['sfsi_plus_textBefor_icons_fontcolor']		=  	(isset($option8['sfsi_plus_textBefor_icons_fontcolor']))
															? $option8['sfsi_plus_textBefor_icons_fontcolor']
															: '#000000';
	$option8['sfsi_plus_icons_DisplayCounts']		= 	(isset($option8['sfsi_plus_icons_DisplayCounts']))
															? sanitize_text_field($option8['sfsi_plus_icons_DisplayCounts'])
															: '';
	$option8['sfsi_plus_rectsub'] 					= 	(isset($option8['sfsi_plus_rectsub']))
															? sanitize_text_field($option8['sfsi_plus_rectsub'])
															: '';
	$option8['sfsi_plus_rectfb'] 					= 	(isset($option8['sfsi_plus_rectfb']))
															? sanitize_text_field($option8['sfsi_plus_rectfb'])
															: '';
	$option8['sfsi_plus_rectgp'] 					= 	(isset($option8['sfsi_plus_rectgp']))
															? sanitize_text_field($option8['sfsi_plus_rectgp'])
															: '';
	$option8['sfsi_plus_rectshr'] 					= 	(isset($option8['sfsi_plus_rectshr']))
															? sanitize_text_field($option8['sfsi_plus_rectshr'])
															: '';
	$option8['sfsi_plus_recttwtr'] 					= 	(isset($option8['sfsi_plus_recttwtr']))
															? sanitize_text_field($option8['sfsi_plus_recttwtr'])
															: '';
	$option8['sfsi_plus_rectpinit'] 				= 	(isset($option8['sfsi_plus_rectpinit']))
															? sanitize_text_field($option8['sfsi_plus_rectpinit'])
															: '';
	$option8['sfsi_plus_rectfbshare'] 				= 	(isset($option8['sfsi_plus_rectfbshare']))
															? sanitize_text_field($option8['sfsi_plus_rectfbshare'])
															: '';
	$option8['sfsi_plus_rectlinkedin'] 				= 	(isset($option8['sfsi_plus_rectlinkedin']))
															? sanitize_text_field($option8['sfsi_plus_rectlinkedin'])
															: '';														
	$option8['sfsi_plus_rectreddit'] 				= 	(isset($option8['sfsi_plus_rectreddit']))
															? sanitize_text_field($option8['sfsi_plus_rectreddit'])
															: '';
	$option8['sfsi_plus_widget_show_on_desktop'] 	= 	(isset($option8['sfsi_plus_widget_show_on_desktop']))
															? sanitize_text_field($option8['sfsi_plus_widget_show_on_desktop'])
															: '';
	$option8['sfsi_plus_widget_show_on_mobile'] 	= 	(isset($option8['sfsi_plus_widget_show_on_mobile']))
															? sanitize_text_field($option8['sfsi_plus_widget_show_on_mobile'])
															: '';

	$option8['sfsi_plus_float_show_on_desktop'] 	= 	(isset($option8['sfsi_plus_float_show_on_desktop']))
															? sanitize_text_field($option8['sfsi_plus_float_show_on_desktop'])
															: '';
	$option8['sfsi_plus_float_show_on_mobile'] 		= 	(isset($option8['sfsi_plus_float_show_on_mobile']))
															? sanitize_text_field($option8['sfsi_plus_float_show_on_mobile'])
															: '';															

	$option8['sfsi_plus_shortcode_show_on_desktop'] = 	(isset($option8['sfsi_plus_shortcode_show_on_desktop']))
															? sanitize_text_field($option8['sfsi_plus_shortcode_show_on_desktop'])
															: '';
	$option8['sfsi_plus_shortcode_show_on_mobile'] 	= 	(isset($option8['sfsi_plus_shortcode_show_on_mobile']))
															? sanitize_text_field($option8['sfsi_plus_shortcode_show_on_mobile'])
															: '';

	$option8['sfsi_plus_beforeafterposts_show_on_desktop'] = 	(isset($option8['sfsi_plus_beforeafterposts_show_on_desktop']))
															? sanitize_text_field($option8['sfsi_plus_beforeafterposts_show_on_desktop'])
															: '';
	$option8['sfsi_plus_beforeafterposts_show_on_mobile'] 	= 	(isset($option8['sfsi_plus_beforeafterposts_show_on_mobile']))
															? sanitize_text_field($option8['sfsi_plus_beforeafterposts_show_on_mobile'])
															: '';

	$option8['sfsi_plus_rectangle_icons_shortcode_show_on_desktop'] = 	(isset($option8['sfsi_plus_rectangle_icons_shortcode_show_on_desktop']))
															? sanitize_text_field($option8['sfsi_plus_rectangle_icons_shortcode_show_on_desktop'])
															: '';
	$option8['sfsi_plus_rectangle_icons_shortcode_show_on_mobile'] 	= 	(isset($option8['sfsi_plus_rectangle_icons_shortcode_show_on_mobile']))
															? sanitize_text_field($option8['sfsi_plus_rectangle_icons_shortcode_show_on_mobile'])
															: '';																														
															
	$option8['sfsi_plus_exclude_home'] 				= 	(isset($option8['sfsi_plus_exclude_home']))
															? sanitize_text_field($option8['sfsi_plus_exclude_home'])
															: '';
	$option8['sfsi_plus_exclude_page'] 				= 	(isset($option8['sfsi_plus_exclude_page']))
															? sanitize_text_field($option8['sfsi_plus_exclude_page'])
															: '';														
	$option8['sfsi_plus_exclude_post'] 				= 	(isset($option8['sfsi_plus_exclude_post']))
															? sanitize_text_field($option8['sfsi_plus_exclude_post'])
															: '';
	$option8['sfsi_plus_exclude_tag'] 				= 	(isset($option8['sfsi_plus_exclude_tag']))
															? sanitize_text_field($option8['sfsi_plus_exclude_tag'])
															: '';
	$option8['sfsi_plus_exclude_category'] 			= 	(isset($option8['sfsi_plus_exclude_category']))
															? sanitize_text_field($option8['sfsi_plus_exclude_category'])
															: '';																													
	$option8['sfsi_plus_exclude_date_archive']		= 	(isset($option8['sfsi_plus_exclude_date_archive']))
															? sanitize_text_field($option8['sfsi_plus_exclude_date_archive'])
															: '';
	$option8['sfsi_plus_exclude_author_archive'] 	= 	(isset($option8['sfsi_plus_exclude_author_archive']))
															? sanitize_text_field($option8['sfsi_plus_exclude_author_archive'])
															: '';
	$option8['sfsi_plus_exclude_search'] 			= 	(isset($option8['sfsi_plus_exclude_search']))
															? sanitize_text_field($option8['sfsi_plus_exclude_search'])
															: '';
	$option8['sfsi_plus_exclude_url'] 				= 	(isset($option8['sfsi_plus_exclude_url']))
															? sanitize_text_field($option8['sfsi_plus_exclude_url'])
															: '';
	$option8['sfsi_plus_marginAbove_postIcon'] 		= 	(isset($option8['sfsi_plus_marginAbove_postIcon']))
															? intval($option8['sfsi_plus_marginAbove_postIcon'])
															: '';
	$option8['sfsi_plus_marginBelow_postIcon'] 		= 	(isset($option8['sfsi_plus_marginBelow_postIcon']))
															? intval($option8['sfsi_plus_marginBelow_postIcon'])
															: '';
    $option8['sfsi_plus_display_after_pageposts'] 	= 	(isset($option8['sfsi_plus_display_after_pageposts']))
															? sanitize_text_field($option8['sfsi_plus_display_after_pageposts'])
															: '';
	$option8['sfsi_plus_switch_exclude_custom_post_types'] = 	(isset($option8['sfsi_plus_switch_exclude_custom_post_types']))
															? sanitize_text_field($option8['sfsi_plus_switch_exclude_custom_post_types'])
															: 'no';
	$option8['sfsi_plus_list_exclude_custom_post_types'] = 	(isset($option8['sfsi_plus_list_exclude_custom_post_types']))
															? unserialize($option8['sfsi_plus_list_exclude_custom_post_types'])
															: array();
	$option8['sfsi_plus_switch_exclude_taxonomies'] = 	(isset($option8['sfsi_plus_switch_exclude_taxonomies']))
															? sanitize_text_field($option8['sfsi_plus_switch_exclude_taxonomies'])
															: 'no';
	$option8['sfsi_plus_list_exclude_taxonomies'] = 	(isset($option8['sfsi_plus_list_exclude_taxonomies']))
															? unserialize($option8['sfsi_plus_list_exclude_taxonomies'])
															: array();												
?>

<div class="tab8">
		
    <h1>A) Location on the page</h1>
    
    <ul class="sfsiplus_icn_listing8">

    	<!--*****************************************************************  Show them via a widget section ***********************************************************-->
		
			<?php @include(SFSI_PLUS_DOCROOT.'/views/subviews/que3/sfsi_option_view8_icons_widget.php'); ?>
        
    	<!--*****************************************************************  Define the location on the page ***********************************************************-->

			<?php @include(SFSI_PLUS_DOCROOT.'/views/subviews/que3/sfsi_option_view8_icons_float.php'); ?>
        
        <!--*****************************************************************  Place round icons manually ****************************************************************-->
			
			<?php @include(SFSI_PLUS_DOCROOT.'/views/subviews/que3/sfsi_option_view8_round_icons_manually.php'); ?>
			
        
        <!--*****************************************************************  Show them before or after posts ************************************************************	-->
			
			<?php @include(SFSI_PLUS_DOCROOT.'/views/subviews/que3/sfsi_option_view8_icons_before_after_posts.php'); ?>

	</ul>
	

    <!--  Place rectangle icons manually STARTS here-->
           <?php @include(SFSI_PLUS_DOCROOT.'/views/subviews/que3/sfsi_option_view8_rectangle_icons_manually.php'); ?>
    <!--  Place rectangle icons manually STARTS here-->
	

    <?php @include(SFSI_PLUS_DOCROOT.'/views/subviews/que3/sfsi_option_view8_exclude_icons_onpages.php'); ?>

    
	<!-- SAVE BUTTON SECTION   --> 
	<div class="save_button">
       <img src="<?php echo SFSI_PLUS_PLUGURL ?>images/ajax-loader.gif" class="loader-img" />
       <?php  $nonce = wp_create_nonce("update_plus_step8"); ?>
        <a  href="javascript:;" id="sfsi_plus_save8" title="Save" data-nonce="<?php echo $nonce;?>">
        	<?php  _e( 'Save', SFSI_PLUS_DOMAIN ); ?>
        </a>
  	</div>
    <!-- END SAVE BUTTON SECTION   -->
	
    <a class="sfsiColbtn closeSec" href="javascript:;">
    	<?php  _e( 'Collapse area', SFSI_PLUS_DOMAIN ); ?>
    </a>
	<label class="closeSec"></label>
    
	<!-- ERROR AND SUCCESS MESSAGE AREA-->
	<p class="red_txt errorMsg" style="display:none"> </p>
	<p class="green_txt sucMsg" style="display:none"> </p>
	<div class="clear"></div>
	
</div>

<?php
function sfsi_premium_isSeletcted($givenVal, $value)
{
	if($givenVal == $value)
		return 'selected="true"';
	else
		return '';
}
?>