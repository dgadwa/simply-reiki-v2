    	
		<li class="sfsiplus_show_via_widget_li">
			
			<div class="radio_section tb_4_ck" onclick="checkforinfoslction(this);"><input name="sfsi_plus_show_via_widget" <?php echo ($option8['sfsi_plus_show_via_widget']=='yes') ?  'checked="true"' : '' ;?>  id="sfsi_plus_show_via_widget" type="checkbox" value="yes" class="styled"  /></div>
			
			<div class="sfsiplus_right_info">
				<p>
					<span class="sfsiplus_toglepstpgspn">
                    	<?php  _e( 'Show them via a widget', SFSI_PLUS_DOMAIN ); ?>
                    </span><br>
                    
                    <?php
                    
                    $_widget_desktop_mobile_setting_style ='';

                    if($option8['sfsi_plus_show_via_widget']=='yes')
					{
						$label_style = 'style="display:block; font-size: 16px;"';
						$_widget_desktop_mobile_setting_style = 'display:block';
					}
					else
					{
						$label_style = 'style="font-size: 16px;"';
					}
					?>
					<label class="sfsiplus_sub-subtitle ckckslctn" <?php echo $label_style;?>>
                    	<?php  _e( 'Go to the widget area and drag & drop it where you want to have it!' , SFSI_PLUS_DOMAIN ); ?>
                    	<a href="<?php echo admin_url('widgets.php');?>">
                    		<?php  _e( 'Click here', SFSI_PLUS_DOMAIN ); ?>
                    	</a> 
                    </label>
				</p>

				<div class="sfsiplus_show_desktop_mobile_setting_li widgetDesktopMobileLi" style="<?php echo esc_attr($_widget_desktop_mobile_setting_style);?>">
				
						<div class="sfsidesktopmbilelabel"><span class="sfsiplus_toglepstpgspn"><?php  _e( 'Show on:', SFSI_PLUS_DOMAIN ); ?></span></div>

						<ul class="sfsiplus_icn_listing8 sfsi_plus_closerli">
						    	
						    	<li class="">
									
									<div class="radio_section tb_4_ck">
						            	<input name="sfsi_plus_widget_show_on_desktop" type="checkbox" value="yes" class="styled" <?php echo ($option8['sfsi_plus_widget_show_on_desktop']=='yes') ?  'checked="true"' : '' ;?>>
						            </div>
									
									<div class="sfsiplus_right_info">
										<p><span class="sfsiplus_toglepstpgspn"><?php  _e( 'Desktop', SFSI_PLUS_DOMAIN ); ?></span></p>
									</div>
								</li>
						        
						        <li class="">
									
									<div class="radio_section tb_4_ck">
						            	<input name="sfsi_plus_widget_show_on_mobile"  type="checkbox" value="yes" class="styled" <?php echo ($option8['sfsi_plus_widget_show_on_mobile']=='yes') ?  'checked="true"' : '' ;?>>
						            </div>

									<div class="sfsiplus_right_info">
										<p><span class="sfsiplus_toglepstpgspn"><?php  _e( 'Mobile', SFSI_PLUS_DOMAIN ); ?></span></p>
									</div>
								</li>
						    </ul>			
				</div>

			</div>

		</li>