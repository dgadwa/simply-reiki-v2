jQuery.noConflict();

jQuery(document).on('click', '.b2s-mail-btn', function () {
    if (isMail(jQuery('#b2s-mail-update-input').val())) {
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            dataType: "json",
            cache: false,
            data: {
                'action': 'b2s_post_mail_update',
                'email': jQuery('#b2s-mail-update-input').val(),
                'lang': jQuery('#user_lang').val()
            }
        });
        jQuery('.b2s-mail-update-area').hide();
        jQuery('.b2s-mail-update-success').show();
    } else {
        jQuery('#b2s-mail-update-input').addClass('error');
    }
    return false;
});


jQuery(window).on("load", function () {
    jQuery('.b2s-faq-area').show();
    if (typeof wp.heartbeat == "undefined") {
        jQuery('#b2s-heartbeat-fail').show();
    }
    jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        dataType: "json",
        cache: false,
        data:{
            'action' : 'b2s_get_faq_entries'
        },
        error: function () {
            jQuery('.b2s-faq-area').hide();
            return false;
        },
        success: function (data) {
            if (data.result == true) {
                jQuery('.b2s-loading-area-faq').hide();
                jQuery('.b2s-faq-content').html(data.content);
            } else {
                jQuery('.b2s-faq-area').hide();
            }
        }
    });

});

/* Content-Widget */
(function (){
    if(jQuery('.b2s-dashboard-multi-widget').length > 0)
    {
        var data = [];

        jQuery.ajax({
            url: ajaxurl,
            type: "GET",
            dataType: "json",
            data: {
                'action': 'b2s_get_multi_widget_content',
            },
            success: function (content) {
                data = content;

                widget.data('position',new Date().getSeconds() % data.length);
                show();

                setInterval(function(){
                    jQuery('.b2s-dashboard-multi-widget .glyphicon-chevron-left').trigger("click");
                },30000);
            }

        });

        var widget = jQuery('.b2s-dashboard-multi-widget');

        jQuery('.b2s-dashboard-multi-widget .glyphicon-chevron-right').on("click",function(){
            widget.data('position',widget.data('position')*1+1);
            show(widget);
        });

        jQuery('.b2s-dashboard-multi-widget .glyphicon-chevron-left').on("click",function(){
            widget.data('position',widget.data('position')*1-1);
            show(widget);
        });

        function show()
        {
            if(widget.data('position') <0)
            {
                widget.data('position',data.length - 1);
            }
            else if(widget.data('position') > data.length-1)
            {
                widget.data('position',0);
            }

            var id = widget.data('position');

            widget.find('.b2s-dashboard-multi-widget-content').html(data[id]['content']);
            widget.find('.b2s-dashboard-h5').text(data[id]['title']);
        }
    }
})();

/* Aktivity-Chart*/
/*jQuery(document).ready(function(){
    if(typeof(google) != "undefined") {
        google.charts.load('current', {packages: ['corechart', 'bar'], 'language': jQuery('#b2s-activity-date-picker').data('language')});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            jQuery.ajax({
                url: ajaxurl,
                type: "GET",
                dataType: "json",
                data: {
                    'action': 'b2s_get_stats',
                    'from': jQuery('#b2s-activity-date-picker').val()
                },
                success: function (content) {
                    var activity_chart_data = [];

                    jQuery(Object.keys(content)).each(function () {
                        var date = new Date(this);
                        activity_chart_data.push([{
                            v: date,
                            f: jQuery('#b2s-activity-date-picker').data('language') == "de" ? date.getDay() + '.' + date.getMonth() + '.' + date.getFullYear() : date.getFullYear() + '-' +  date.getMonth() + '-' + date.getDay()
                        }, content[this][0], content[this][1]]);
                    });

                    var data = new google.visualization.DataTable();
                    data.addColumn('date', 'Date');
                    data.addColumn('number', jQuery("#chart_div").data('text-published'))
                    data.addColumn('number', jQuery("#chart_div").data('text-scheduled'))

                    data.addRows(activity_chart_data);

                    var options = {
                        title: '',
                        legend: {position: 'bottom'},
                        vAxis: {
                            format: '#'
                        },
                        colors: ['#79B232', '#dddddd']
                    };

                    var chart = new google.visualization.ColumnChart(
                        document.getElementById('chart_div'));

                    chart.draw(data, options);
                }

            });
        }

        jQuery('#b2s-activity-date-picker').b2sdatepicker({
            'autoClose': true,
            'toggleSelected': true,
            'minutesStep': 15
        });
        jQuery('#b2s-activity-date-picker').on("selectDate", function () {
            jQuery('#chart_div').html("<div class=\"b2s-loading-area\">\n" +
                "        <br>\n" +
                "        <div class=\"b2s-loader-impulse b2s-loader-impulse-md\"></div>\n" +
                "        <div class=\"clearfix\"></div>\n" +
                "    </div>");

            //Warten bis das Event durch ist...
            setTimeout(drawBasic);
        });
    }
});*/



function isMail(mail) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(mail);
}

