<?php 

add_action( 'wp_enqueue_scripts', 'phlox_parent_theme_enqueue_styles' );

function phlox_parent_theme_enqueue_styles() {
    wp_enqueue_script( 'fonts-script', 
    	//Fonts.com - Hello Beautiful
        '//fast.fonts.net/jsapi/63621498-6f6e-4431-93fa-6820ad4351d5.js',
        array(  ), ' ', true
    );
    
    //Load custom CSS files
	wp_enqueue_style( 'phlox-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'phlox-child-styles',
    	get_stylesheet_directory_uri() . '/css/dg-styles.css',
        array( 'phlox-style' )
    );
    
    //Conditional statement below can be used to load scripts on a specific page
    if (is_front_page()) {
	//This area below can be used to test custom js in the future
    wp_enqueue_script( 'test-script', 
        get_stylesheet_directory_uri() . '/dg-scripts/test.js',
        array( 'jquery' ), ' ', true
    );
	}
}




